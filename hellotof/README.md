# HelloTof sample using the HAL3 interface

## Build sample project:
-----------------------
1. Requires the voxl-emulator (found [here](https://gitlab.com/voxl-public/voxl-docker)) to run docker ARM image
    * cd [Path To]/voxl-docker
    * voxl-docker -d [Path To]/apps-proc-examples/hellotof
2. Build project binary:
    * make
3. Push binary to target:
    * adb push hello_hal3_tof /bin
4. Run on target:
    * adb shell
    * hello_hal3_tof -h
    * hello_hal3_tof -c 1 -W 224 -H 1557 -f 2 -d 5 -tfr 10 (assumes -c 1 is the TOF camera)
    * Press Ctrl+C to stop the program anytime
    * Saved images will be dumped to the current directory on target

