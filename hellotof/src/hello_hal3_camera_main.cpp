/*******************************************************************************************************************************
 *
 * Copyright (c) 2019 ModalAI, Inc.
 *
 ******************************************************************************************************************************/
#include <errno.h>
#include <getopt.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hello_hal3_camera.h"
#include "TOFInterface.h"

// Function prototypes
void PrintHelpMessage();
int  ErrorCheck(int numInputsScanned, const char* pOptionName);
int  ParseArgs(int         argc,
               char* const pArgv[],
               int*        pCameraid,
               int*        pWidth,
               int*        pHeight,
               int*        pFormat,
               int*        pTofDatatype,
               int*        pTofNumFrames,
               int*        pDumpPreviewFrames);

void CtrlCHandler(int dummy);

volatile int g_keepRunning = 1;

// -----------------------------------------------------------------------------------------------------------------------------
// Main
// -----------------------------------------------------------------------------------------------------------------------------
int main(int argc, char* const argv[])
{
    CameraHAL3* pCameraHAL3 = new CameraHAL3;
    int         status;
    // All the default values can be overwritten with the command line args. If the command line args is not specified, the
    // defaults are used
    int         cameraid          = 0;
    int         width             = 640;
    int         height            = 480;
    int         format            = PreviewFormatNV21;
    int         mode              = CameraModePreview;
    int         tofdatatype       = 1 << tof::RoyaleListenerType::LISTENER_IR_IMAGE;
    int         tofnumframes      = 0;
    int         dumpPreviewFrames = 0;

    signal(SIGINT, CtrlCHandler);

    status = ParseArgs(argc, argv, &cameraid, &width, &height, &format, &tofdatatype, &tofnumframes, &dumpPreviewFrames);

    if (status == 0)
    {
        printf("\nCamera id: %d", cameraid);
        printf("\nImage width: %d", width);
        printf("\nImage height: %d", height);
        printf("\nNumber of frames to dump: %d", dumpPreviewFrames);
        printf("\nCamera mode: preview");

        status = pCameraHAL3->Initialize();
    }
    else
    {
        PrintHelpMessage();
    }

    if (status == 0)
    {
        // Making this function call will start the camera (cameraid) and it will start streaming frames
        status = pCameraHAL3->Start(cameraid,
                                    width,
                                    height,
                                    (PreviewFormat)format,
                                    (CameraMode)mode,
                                    tofdatatype,
                                    tofnumframes,
                                    NULL,
                                    dumpPreviewFrames);
    }

    if (status == 0)
    {
        printf("\n\nCamera HAL3 test is now running");
        fflush(stdout);

        // The camera keeps producing frames till the user presses Ctrl+C to terminate the program
        while (g_keepRunning)
        {
            usleep(1e5);
        }

        printf("\nCamera HAL3 test is now stopping");
        pCameraHAL3->Stop();
        printf("\nCamera HAL3 test is done");
    }

    delete pCameraHAL3;

    printf("\n\n");
    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Parses the command line arguments to the main function
// -----------------------------------------------------------------------------------------------------------------------------
int ParseArgs(int         argc,
              char* const pArgv[],
              int*        pCameraid,
              int*        pWidth,
              int*        pHeight,
              int*        pFormat,
              int*        pTofDatatype,
              int*        pTofNumFrames,
              int*        pDumpPreviewFrames)
{
    static struct option LongOptions[] =
    {
        {"cameraid",     optional_argument, 0, 'c'},
        {"width",        optional_argument, 0, 'W'},
        {"height",       optional_argument, 0, 'H'},
        {"format",       optional_argument, 0, 'f'},
        {"dumppreview",  optional_argument, 0, 'd'},  
        {"tof",          required_argument, 0, 't'},
        {"tfr",          required_argument, 0,  0 },
        {"help",         no_argument,       0, 'h'},
        {0, 0, 0, 0                               }
    };

    int numInputsScanned = 0;
    int optionIndex      = 0;
    int status           = 0;    
    int totalTofDatatype = 0;
    int option;

    *pTofNumFrames = 0;

    while ((status == 0) && (option = getopt_long_only (argc, pArgv, ":c:W:H:f:m:v:d:t:h", &LongOptions[0], &optionIndex)) != -1)
    {
        switch(option)
        {
            case 0:
                numInputsScanned = sscanf(optarg, "%d", pTofNumFrames);

                if (ErrorCheck(numInputsScanned, LongOptions[optionIndex].name) != 0)
                {
                    printf("\nNo TOF frames specified!");
                    status = -EINVAL;
                }

                break;

            case 'c':
                numInputsScanned = sscanf(optarg, "%d", pCameraid);
                
                if (ErrorCheck(numInputsScanned, LongOptions[optionIndex].name) != 0)
                {
                    printf("\nNo cameraId specified!");
                    status = -EINVAL;
                }
               
                break;

            case 'W':
                numInputsScanned = sscanf(optarg, "%d", pWidth);
                
                if (ErrorCheck(numInputsScanned, LongOptions[optionIndex].name) != 0)
                {
                    printf("\nNo width specified");
                    status = -EINVAL;
                }
                
                break;

            case 'H':
                numInputsScanned = sscanf(optarg, "%d", pHeight);
                
                if (ErrorCheck(numInputsScanned, LongOptions[optionIndex].name) != 0)
                {
                    printf("\nNo height specified");
                    status = -EINVAL;
                }
                
                break;

            case 'd':
                numInputsScanned = sscanf(optarg, "%d", pDumpPreviewFrames);
                
                if (ErrorCheck(numInputsScanned, LongOptions[optionIndex].name) != 0)
                {
                    printf("\nNo preview dump frames specified");
                    status = -EINVAL;
                }
                
                break;

            case 'f':
                numInputsScanned = sscanf(optarg, "%d", pFormat);
                
                if (ErrorCheck(numInputsScanned, LongOptions[optionIndex].name) == 0)
                {
                    if ((*pFormat != PreviewFormatNV21) && (*pFormat != PreviewFormatRAW8) && (*pFormat != PreviewFormatBLOB))
                    {
                        printf("\nUnsupported format specified!");
                        status = -EINVAL;
                    }
                }
                else
                {
                    printf("\nNo format specified");
                    status = -EINVAL;
                }
                
                break;

            case 't':
            {
                char  temp[80];
                char* pDataType;
                int   datatype;
                numInputsScanned = sscanf(optarg, "%s", &temp[0]);

                if (ErrorCheck(numInputsScanned, LongOptions[optionIndex].name) != 0)
                {
                    printf("\nNo Tof datatype specified!");
                    status = -EINVAL;
                }
                else
                {
                    pDataType = strtok(temp, ",");
                    *pTofDatatype = 0;
                    printf("\nChosen datatypes:");

                    while (pDataType != NULL)
                    {
                        sscanf(pDataType, "%d", &datatype);

                        if (datatype < (int)tof::MaxRoyaleListenerTypes)
                        {
                            totalTofDatatype++;

                            *pTofDatatype |= (1 << datatype);
                            pDataType = strtok(NULL, ",");

                            switch ((tof::RoyaleListenerType)(1 << datatype))
                            {
                                case tof::LISTENER_DEPTH_DATA:
                                    printf("\n\tDEPTH_DATA");
                                    break;

                                case tof::LISTENER_SPARSE_POINT_CLOUD:
                                    printf("\n\tSPARSE_POINT_CLOUD");
                                    break;

                                case tof::LISTENER_DEPTH_IMAGE:
                                    printf("\n\tDEPTH_IMAGE");
                                    break;

                                case tof::LISTENER_IR_IMAGE:
                                    printf("\n\tIR_IMAGE");
                                    break;

                                default:
                                    break;
                            }
                        }
                        else
                        {
                            printf("\nBad Tof datatype specified - %d is not a valid value!", datatype);
                            status = -EINVAL;
                            *pTofDatatype = 0;
                            break;
                        }
                    }
                }
            }
            break;

            case 'h':
                status = -EINVAL; // This will have the effect of printing the help message and exiting the program
                break;

            // Unknown argument
            case '?':
            default:
                printf("\nInvalid argument passed!");
                status = -EINVAL;
                break;
        }
    }

    // The pTofNumFrames is for each datatype, so multiply by the totalTofDatatype to get the total count
    *pTofNumFrames *= totalTofDatatype;

    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Print the help message
// -----------------------------------------------------------------------------------------------------------------------------
void PrintHelpMessage()
{
    printf("\n\nCommand line arguments are as follows:\n");
    printf("\n-c : camera id (Default 0)");
    printf("\n-W : frame width (Default 640)");
    printf("\n-H : frame height (Default 480)");
    printf("\n-f : frame format (Default preview format nv21)");
    printf("\n\t0: NV21");
    printf("\n\t1: Raw8");
    printf("\n\t2: Blob (For TOF camera)");
    printf("\n-tof : TOF datatype (IR Image by default)");
    printf("\n\t0: Depth Data");
    printf("\n\t1: Sparse Point Cloud");
    printf("\n\t2: Depth Image");
    printf("\n\t3: IR Image");
    printf("\n\tYou can give multiple outputs like: 0,2,3. No spaces before/after ','");
    printf("\n-tfr : Dump 'n' TOF post-processed frames (Default 0)");
    printf("\n-d : Dump 'n' preview frames (Default 0)");
    printf("\n-h : Print this help message");
    printf("\n\nFor example: hello_hal3_tof -c 1 -W 224 -H 1557 -f 2 -d 5 -tfr 10");
}

// -----------------------------------------------------------------------------------------------------------------------------
// Check for error in parsing the arguments
// -----------------------------------------------------------------------------------------------------------------------------
int ErrorCheck(int numInputsScanned, const char* pOptionName)
{
    int error = 0;

    if (numInputsScanned != 1)
    {
        fprintf(stderr, "ERROR: Invalid argument for %s option\n", pOptionName);
        error = -1;
    }

    return error;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Ctrl+C handler that will stop the camera and exit the program gracefully
// -----------------------------------------------------------------------------------------------------------------------------
void CtrlCHandler(int dummy)
{
    g_keepRunning = 0;
    signal(SIGINT, NULL);
}