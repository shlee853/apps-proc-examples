/*******************************************************************************************************************************
 *
 * Copyright (c) 2019 ModalAI, Inc.
 *
 ******************************************************************************************************************************/
#ifndef __TOF_INTERFACE_H
#define __TOF_INTERFACE_H

#include <stdint.h>

namespace tof
{

// -----------------------------------------------------------------------------------------------------------------------------
// These are the post processed forms of TOF camera data
// -----------------------------------------------------------------------------------------------------------------------------
typedef enum
{
    LISTENER_NONE               = 0x0,
    LISTENER_DEPTH_DATA         = 0x1,
    LISTENER_SPARSE_POINT_CLOUD = 0x2,
    LISTENER_DEPTH_IMAGE        = 0x4,
    LISTENER_IR_IMAGE           = 0x8
} RoyaleListenerType;

static const uint32_t MaxRoyaleListenerTypes = 4;

// -----------------------------------------------------------------------------------------------------------------------------
// This is the listener client that the TOF bridge library will call when it has post processed data from the Royale PMD libs
// -----------------------------------------------------------------------------------------------------------------------------
class IRoyaleDataListener
{
public:
    IRoyaleDataListener() {};
    virtual ~IRoyaleDataListener() {};
    virtual bool RoyaleDataDone(const void* pData, uint32_t size, int64_t timestamp, RoyaleListenerType dataType) = 0;
};

}

// -----------------------------------------------------------------------------------------------------------------------------
// TOF Initialization data
// -----------------------------------------------------------------------------------------------------------------------------
struct TOFInitializationData
{
    void*                     pTOFInterface;        ///< TOF Interface pointer
    uint32_t                  numDataTypes;         ///< Type of listener types
    tof::RoyaleListenerType*  pDataTypes;           ///< tof::RoyaleListenerType
    tof::IRoyaleDataListener* pListener;            ///< Class object of type tof::IRoyaleDataListener
    uint8_t                   frameRate;            ///< TOF camera Frame rate
};

// -----------------------------------------------------------------------------------------------------------------------------
// These are the main interfaces to the TOF Bridge library
// -----------------------------------------------------------------------------------------------------------------------------
extern "C" {
void* TOFCreateInterface();
int   TOFInitialize(TOFInitializationData* pTOFInitializationData);
void  TOFProcessRAW16(void* pTOFInterface, uint16_t* pRaw16PixelData, uint64_t timestamp);
void  TOFDestroyInterface(void* pTOFInterface);
}

#endif // __TOF_INTERFACE_H