/*******************************************************************************************************************************
 *
 * Copyright (c) 2019 ModalAI, Inc.
 *
 ******************************************************************************************************************************/

#ifndef HELLO_HAL3_CAMERA
#define HELLO_HAL3_CAMERA

#include <camera/CameraMetadata.h>
#include <hardware/camera3.h>
#include "TOFInterface.h"
#include <list>

// Forward Declaration
class BufferManager;
class PerCameraMgr;

// -----------------------------------------------------------------------------------------------------------------------------
// Thread Data for camera request and result thread
// -----------------------------------------------------------------------------------------------------------------------------
typedef struct ThreadData
{
    pthread_t         thread;                   ///< Thread handle
    pthread_mutex_t   mutex;                    ///< Mutex for list access
    pthread_cond_t    cond;                     ///< Condition variable for wake up
    std::list<void*>  msgQueue;                 ///< Message queue
    PerCameraMgr*     pCameraMgr;               ///< Pointer to the per camera mgr class
    camera3_device_t* pDevice;                  ///< Camera device that the thread is communicating with
    void*             pPrivate;                 ///< Any private information if need be
    volatile bool     stop;                     ///< Indication for the thread to terminate
    volatile int      lastResultFrameNumber;    ///< Last frame the capture result thread should wait for before terminating
} ThreadData;

// -----------------------------------------------------------------------------------------------------------------------------
// Different camera modes
// -----------------------------------------------------------------------------------------------------------------------------
enum CameraMode
{
    CameraModePreview = 0,      ///< Preview only mode
    CameraModeVideo   = 1,      ///< Preview + Video mode
};

// -----------------------------------------------------------------------------------------------------------------------------
// Supported preview formats
// -----------------------------------------------------------------------------------------------------------------------------
enum PreviewFormat
{
    PreviewFormatNV21 = 0,      ///< NV21
    PreviewFormatRAW8 = 1,      ///< RAW8 (If the camera doesnt support RAW8, RAW10 will be requested and converted to RAW8)
    PreviewFormatBLOB = 2,      ///< BLOB (TOF camera uses this for preview format)    
};

// -----------------------------------------------------------------------------------------------------------------------------
// Supported stream types
// -----------------------------------------------------------------------------------------------------------------------------
enum StreamType
{
    StreamTypePreview = 0,      ///< Preview stream
    StreamTypeVideo,            ///< Video stream
    StreamTypeMax,
};

// -----------------------------------------------------------------------------------------------------------------------------
// Supported stream types
// -----------------------------------------------------------------------------------------------------------------------------
typedef enum
{
    // RAW only mode for devices that will simultaneously use more than two cameras.
    // This mode has following limitations: Back end 3A, Face Detect or any additional functionality depending on image/sensor
    // statistics and YUV streams will be disabled
    
    QCAMERA3_VENDOR_STREAM_CONFIGURATION_RAW_ONLY_MODE = 0x8000,
} QCamera3VendorStreamConfiguration;

//------------------------------------------------------------------------------------------------------------------------------
// Everything needed to handle a single camera
//------------------------------------------------------------------------------------------------------------------------------
class PerCameraMgr : public tof::IRoyaleDataListener
{
public:
    PerCameraMgr();
    ~PerCameraMgr() { }

    // Return the number of preview frames to dump to files
    int GetDumpPreviewFrames() const
    {
        return m_dumpPreviewFrames;
    }
    // Return the TOF interface object
    void* GetTOFInterface()
    {
        return m_pTOFInterface;
    }
    // Perform any one time initialization
    int Initialize(const camera_module_t* pCameraModule,
                   const camera_info*     pCameraInfo,
                   int                    cameraid,
                   int                    width,
                   int                    height,
                   PreviewFormat          format,
                   CameraMode             mode,
                   int                    tofdatatype,
                   int                    tofnumframes,
                   const char*            pVideoFilename,
                   int                    dumpPreviewFrames);
    // Start the camera so that it starts streaming frames
    int  Start();
    // Stop the camera and stop sending any more requests to the camera module
    void Stop();
    // Send one capture request to the camera module
    int  ProcessOneCaptureRequest(int frameNumber);
    // Process one capture result sent by the camera module
    void ProcessOneCaptureResult(const camera3_capture_result* pHalResult);
    // The request thread calls this function to indicate it sent the last request to the camera module and will not send any
    // more requests
    void StoppedSendingRequest(int framenumber);
    // Callback function for the TOF bridge to provide the post processed TOF data
    bool RoyaleDataDone(const void*             pData,
                        uint32_t                size,
                        int64_t                 timestamp,
                        tof::RoyaleListenerType dataType);

private:
    // Is the camera running in preview + video mode
    bool IsVideoMode() const
    {
        return (m_cameraMode == CameraModeVideo);
    }
    // Return if this resolution corresponds to a TOF camera
    void DetermineTOFCamera(int width, int height)
    {
        if ((width == 224) && (height == 1557))
        {
            m_isTOF = true;
        }
        else
        {
            m_isTOF = false;
        }
    }
    // Check for TOF camera
    bool IsTOFCamera()
    {
        return m_isTOF;
    }

    // Camera module calls this function to pass on the capture frame result
    static void CameraModuleCaptureResult(const camera3_callback_ops *cb,const camera3_capture_result *hal_result);
    // Camera module calls this function to notify us of any messages
    static void CameraModuleNotify(const struct camera3_callback_ops *cb, const camera3_notify_msg_t *msg);
    // Check the static camera characteristics for a matching (w, h, format) combination
    bool        IsStreamConfigSupported(int width, int height, int format);
    // Call the cameram module and pass it the stream configuration
    int         ConfigureStreams();
    // Allocate the stream buffers using the BufferManager
    int         AllocateStreamBuffers();
    // Call the camera module to get the default camera settings
    void        ConstructDefaultRequestSettings();

    static const uint32_t MaxPreviewBuffers = 16;
    static const uint32_t MaxVideoBuffers   = 16;
    static const uint8_t  FrameRate         = 30;

    // camera3_callback_ops is returned to us in every result callback. We piggy back any private information we may need at
    // the time of processing the frame result. When we register the callbacks with the camera module, we register the starting
    // address of this structure (which is camera3_callbacks_ops) but followed by our private information. When we receive a
    // pointer to this structure at the time of capture result, we typecast the incoming pointer to this structure type pointer
    // and access our private information
    struct Camera3Callbacks
    {
        camera3_callback_ops cameraCallbacks;
        void* pPrivate;
    };

    int32_t                        m_cameraId;                      ///< Camera id
    CameraMode                     m_cameraMode;                    ///< Camera mode
    int32_t                        m_previewFormat;                 ///< Preview format
    Camera3Callbacks               m_cameraCallbacks;               ///< Camera callbacks
    const camera_module_t*         m_pCameraModule;                 ///< Camera module
    const camera_info*             m_pCameraInfo;                   ///< Camera info
    int                            m_width;                         ///< Width
    int                            m_height;                        ///< Height 
    int                            m_tofdatatype;                   ///< TOF datatype
    int                            m_tofnumframes;                  ///< Number of TOF frames to dump
    const char*                    m_pVideoFilename;                ///< Video filename
    FILE*                          m_pVideoFilehandle;              ///< Video file handle
    int                            m_dumpPreviewFrames;             ///< Number of preview frames to dump
    int64_t                        m_exposureSetting;               ///< Exposure setting
    int32_t                        m_gainSetting;                   ///< Gain setting
    camera3_stream_t               m_streams[StreamTypeMax];        ///< Streams to be used for the camera request
    camera3_device_t*              m_pDevice;                       ///< HAL3 device
    android::CameraMetadata        m_requestMetadata;               ///< Per request metadata
    BufferManager*                 m_pBufferManager[StreamTypeMax]; ///< Buffer manager per stream
    ThreadData                     m_requestThread;                 ///< Request thread private data
    ThreadData                     m_resultThread;                  ///< Result Thread private data
    bool                           m_isTOF;                         ///< Is this a TOF camera
    void*                          m_pTOFInterface;                 ///< TOF interface to process the TOF camera raw data
};

//------------------------------------------------------------------------------------------------------------------------------
// Main interface class to manage all the cameras
//------------------------------------------------------------------------------------------------------------------------------
class CameraHAL3
{
public:
    CameraHAL3();
    ~CameraHAL3() { }
    // Perform any one time initialization
    int Initialize();
    // Start a camera with the requested parameters
    int Start(int           cameraid,
              int           width,
              int           height,
              PreviewFormat format,
              CameraMode    mode,
              int           tofdatatype,
              int           tofnumframes,
              const char*   pVideoFilename,
              int           dumpPreviewFrames);
    // Stop the opened camera and close it
    void Stop();

private:
    // Callback to indicate device status change
    static void CameraDeviceStatusChange(const struct camera_module_callbacks* callbacks, int camera_id, int new_status)
    {
    }
    // Callback to indicate torch mode status change
    static void TorchModeStatusChange(const struct camera_module_callbacks* callbacks, const char* camera_id, int new_status)
    {
    }

    // Load the camera module to start communicating with it
    int LoadCameraModule();

    static const uint32_t MaxCameras = 4;

    camera_module_t*        m_pCameraModule;                ///< Camera module
    camera_module_callbacks m_moduleCallbacks;              ///< Camera module callbacks
    PerCameraMgr*           m_pPerCameraMgr[MaxCameras];    ///< Each instance manages one camera
    int32_t                 m_numCameras;                   ///< Number of cameras detected
    camera_info             m_cameraInfo[MaxCameras];       ///< Per camera info
};

#endif // HELLO_HAL3_CAMERA
