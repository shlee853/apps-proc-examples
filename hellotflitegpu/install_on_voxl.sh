#!/bin/bash
################################################################################
# Copyright (c) 2020 ModalAI, Inc. All rights reserved.
#
# Installs the ipk package on target.
# Requires the ipk to be built and an adb connection.
#
################################################################################
set -e

EXECUTABLE=hello-voxl-tflite-gpu
echo "searching for ADB device"
adb wait-for-device
echo "adb device found"

echo "pushing dnn directory on target to /bin/dnn/"
adb push dnn /bin/dnn
echo "pushing hello-voxl-tflite-gpu executable to /usr/bin/"
adb push $EXECUTABLE /usr/bin
