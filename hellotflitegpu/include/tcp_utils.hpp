/*
 * Copyright (c) 2015-2016 Qualcomm Technologies, Inc.  All Rights Reserved.
 * Qualcomm Technologies Proprietary and Confidential.
 */

#ifndef TCP_UTILS_HPP_
#define TCP_UTILS_HPP_

#include <arpa/inet.h>
#include <errno.h>
#include <iostream>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#include "crc16.h"


struct ImageHeader
{
  uint8_t message_id;
  uint8_t flag;
  int32_t frame_id;
  int64_t timestamp_ns;
  uint16_t num_cols;
  uint16_t num_rows;
  uint8_t opts[4];
  uint16_t checksum;
};


class TcpServer
{
 public:
  int create_socket (const char *ip_addr, int port_num);
  int bind_socket (void);
  int check_socket (void);
  int connect_client (void);
  int recv_message (void);
  int send_message (const uint8_t *msg, size_t msg_len,
      int msg_id, uint16_t num_cols, uint16_t num_rows,
      uint8_t *opts, int32_t frame, int64_t timestamp);
  int send_message (const uint16_t *msg, size_t msg_len,
      int msg_id, uint16_t num_cols, uint16_t num_rows,
      uint8_t *opts, int32_t frame, int64_t timestamp);
  int send_message (const float *msg, size_t msg_len,
      int msg_id, uint16_t num_cols, uint16_t num_rows,
      uint8_t *opts, int32_t frame, int64_t timestamp);
  ~TcpServer (void);

 private:
  int sock_;
  int data_sock_;
  size_t pack_len_;
  struct sockaddr_in serv_addr_;
  socklen_t serv_addr_size_;
  fd_set master_;
  fd_set readfds_;
};

#endif // TCP_UTILS_HPP_
