/*******************************************************************************************************************************
 *
 * Copyright (c) 2019 ModalAI, Inc.
 *
 ******************************************************************************************************************************/
#ifndef HELLO_HAL3_CAMERA_TFLITE
#define HELLO_HAL3_CAMERA_TFLITE

struct BufferInfo;
class  TcpServer;
class  VideoEncoder;

// -----------------------------------------------------------------------------------------------------------------------------
// Tensorflow thread message data
// -----------------------------------------------------------------------------------------------------------------------------
struct TensorflowMessage
{
    BufferInfo* pBufferInfo;     ///< Preview buffer information
    int         frameNumber;     ///< Frame number associated with the preview buffer
};

// -----------------------------------------------------------------------------------------------------------------------------
// Tensorflow thread private data
// -----------------------------------------------------------------------------------------------------------------------------
struct TFliteThreadPrivateData
{
    TcpServer*    pTcpServer;             ///< TCP Server for streaming image
    VideoEncoder* pVideoEncoder;          ///< Video encoder for encoding the video frames
    char*         pDnnModelFile;          ///< Dnn Model filename
    char*         pLabelsFile;            ///< DNN Model labels filename
};

#endif // HELLO_HAL3_CAMERA_TFLITE