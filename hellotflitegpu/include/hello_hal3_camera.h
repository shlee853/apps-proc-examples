/*******************************************************************************************************************************
 *
 * Copyright (c) 2019 ModalAI, Inc.
 *
 ******************************************************************************************************************************/

#ifndef HELLO_HAL3_CAMERA
#define HELLO_HAL3_CAMERA

#include <camera/CameraMetadata.h>
#include <hardware/camera3.h>
#include <list>
#include "tflite.h"

// Forward Declaration
class BufferManager;
class PerCameraMgr;
class VideoEncoder;
class TcpServer;

struct VideoEncoderConfig;

// -----------------------------------------------------------------------------------------------------------------------------
// Different camera modes
// -----------------------------------------------------------------------------------------------------------------------------
enum CameraMode
{
    CameraModePreview = 0,      ///< Preview only mode
    CameraModeVideo   = 1,      ///< Preview + Video mode
};

// -----------------------------------------------------------------------------------------------------------------------------
// Supported preview formats
// -----------------------------------------------------------------------------------------------------------------------------
enum PreviewFormat
{
    PreviewFormatNV21 = 0,      ///< NV21
    PreviewFormatRAW8 = 1,      ///< RAW8 (If the camera doesnt support RAW8, RAW10 will be requested and converted to RAW8)
    PreviewFormatBLOB = 2,      ///< BLOB (TOF camera uses this for preview format)
};

// -----------------------------------------------------------------------------------------------------------------------------
// Supported stream types
// -----------------------------------------------------------------------------------------------------------------------------
enum StreamType
{
    StreamTypePreview = 0,      ///< Preview stream
    StreamTypeVideo,            ///< Video stream
    StreamTypeMax,
};

// -----------------------------------------------------------------------------------------------------------------------------
// Supported stream types
// -----------------------------------------------------------------------------------------------------------------------------
typedef enum
{
    // RAW only mode for devices that will simultaneously use more than two cameras.
    // This mode has following limitations: Back end 3A, Face Detect or any additional functionality depending on image/sensor
    // statistics and YUV streams will be disabled
    
    QCAMERA3_VENDOR_STREAM_CONFIGURATION_RAW_ONLY_MODE = 0x8000,
} QCamera3VendorStreamConfiguration;

// -----------------------------------------------------------------------------------------------------------------------------
// Thread Data for camera request and result thread
// -----------------------------------------------------------------------------------------------------------------------------
typedef struct ThreadData
{
    pthread_t         thread;                   ///< Thread handle
    pthread_mutex_t   mutex;                    ///< Mutex for list access
    pthread_cond_t    cond;                     ///< Condition variable for wake up
    std::list<void*>  msgQueue;                 ///< Message queue
    PerCameraMgr*     pCameraMgr;               ///< Pointer to the per camera mgr class
    camera3_device_t* pDevice;                  ///< Camera device that the thread is communicating with
    void*             pPrivate;                 ///< Any private information if need be
    volatile bool     stop;                     ///< Indication for the thread to terminate
    volatile int      lastResultFrameNumber;    ///< Last frame the capture result thread should wait for before terminating
} ThreadData;

// -----------------------------------------------------------------------------------------------------------------------------
// Per camera initialization data
// -----------------------------------------------------------------------------------------------------------------------------
typedef struct PerCameraInitData
{
    const camera_module_t*  pCameraModule;          ///< Camera module
    const camera_info*      pCameraInfo;            ///< Info about the camera managed by this class
    int                     cameraid;               ///< Camera id to open
    int                     width;                  ///< Image buffer width
    int                     height;                 ///< Image buffer height
    PreviewFormat           format;                 ///< Image buffer format
    CameraMode              mode;                   ///< Preview / Video
    int                     faceDetect;             ///< Face detect
    int                     dnn;                    ///< DNN
    int                     armcl;                  ///< ARMCL
    const char*             pVideoFilename;         ///< Video filename for Video mode
    int                     dumpPreviewFrames;      ///< Number of preview frames to dump
    char*                   pIPAddress;             ///< IP address to stream the RGB stream to
    char*                   pDnnModelFile;          ///< Dnn Model filename
    char*                   pLabelsFile;            ///< DNN Model labels filename
} PerCameraInitData;

//------------------------------------------------------------------------------------------------------------------------------
// Everything needed to handle a single camera
//------------------------------------------------------------------------------------------------------------------------------
class PerCameraMgr
{
public:
    PerCameraMgr();
    ~PerCameraMgr() { }

    // Return the number of preview frames to dump to files
    int GetDumpPreviewFrames() const
    {
        return m_dumpPreviewFrames;
    }
    // Return the VideoEncoder object
    VideoEncoder* GetVideoEncoder()
    {
        return m_pVideoEncoder;
    }
    // Is the camera running in preview + video mode
    bool IsVideoMode() const
    {
        return (m_cameraMode == CameraModeVideo);
    }
    // Is FaceDetect enabled
    bool IsFaceDetectEnabled() const
    {
        return (m_faceDetect == 1) ? true : false;
    }
    // Is DNN enabled
    bool IsDNNEnabled() const
    {
        return (m_dnn == 1) ? true : false;
    }
    // Is ARMCL enabled
    bool IsARMCLEnabled() const
    {
        return (m_armcl == 1) ? true : false;
    }
    // Get the FaceDetect thread data
    ThreadData* TensorflowLiteThreadData()
    {
        return &m_tensorflowliteThread;
    }
    // Is live streaming enabled
    bool IsStreamingEnabled()
    {
        return m_isStreamingEnabled;
    }

    // Get the Tcp server to use to transmit the live stream
    TcpServer* GetTcpServer()
    {
        return m_pTcpServer;
    }

    // Perform any one time initialization
    int Initialize(PerCameraInitData* pInitData);
    // Start the camera so that it starts streaming frames
    int  Start();
    // Stop the camera and stop sending any more requests to the camera module
    void Stop();
    // Send one capture request to the camera module
    int  ProcessOneCaptureRequest(int frameNumber);
    // Process one capture result sent by the camera module
    void ProcessOneCaptureResult(const camera3_capture_result* pHalResult);
    // The request thread calls this function to indicate it sent the last request to the camera module and will not send any
    // more requests
    void StoppedSendingRequest(int framenumber);

private:
    // Camera module calls this function to pass on the capture frame result
    static void CameraModuleCaptureResult(const camera3_callback_ops *cb,const camera3_capture_result *hal_result);
    // Camera module calls this function to notify us of any messages
    static void CameraModuleNotify(const struct camera3_callback_ops *cb, const camera3_notify_msg_t *msg);
    // Check the static camera characteristics for a matching (w, h, format) combination
    bool        IsStreamConfigSupported(int width, int height, int format);
    // Call the cameram module and pass it the stream configuration
    int         ConfigureStreams();
    // Allocate the stream buffers using the BufferManager
    int         AllocateStreamBuffers();
    // Call the camera module to get the default camera settings
    void        ConstructDefaultRequestSettings();

    const uint32_t MaxPreviewBuffers = 16;
    const uint32_t MaxVideoBuffers   = 16;
    const uint32_t VideoImageFormat  = HAL_PIXEL_FORMAT_YCbCr_420_888;
    const int32_t  FrameRate[2]      = {30, 30};

    // camera3_callback_ops is returned to us in every result callback. We piggy back any private information we may need at
    // the time of processing the frame result. When we register the callbacks with the camera module, we register the starting
    // address of this structure (which is camera3_callbacks_ops) but followed by our private information. When we receive a
    // pointer to this structure at the time of capture result, we typecast the incoming pointer to this structure type pointer
    // and access our private information
    struct Camera3Callbacks
    {
        camera3_callback_ops cameraCallbacks;
        void* pPrivate;
    };

    int32_t                        m_cameraId;                      ///< Camera id
    CameraMode                     m_cameraMode;                    ///< Camera mode
    int32_t                        m_previewFormat;                 ///< Preview format
    Camera3Callbacks               m_cameraCallbacks;               ///< Camera callbacks
    const camera_module_t*         m_pCameraModule;                 ///< Camera module
    const camera_info*             m_pCameraInfo;                   ///< Camera info
    int                            m_width;                         ///< Width
    int                            m_height;                        ///< Height 
    int                            m_faceDetect;                    ///< Enable/Disable facedetect
    int                            m_dnn;                           ///< Enable/Disable DNN
    int                            m_armcl;                         ///< Enable/Disable ARMCL
    const char*                    m_pVideoFilename;                ///< Video filename
    FILE*                          m_pVideoFilehandle;              ///< Video file handle
    VideoEncoder*                  m_pVideoEncoder;                 ///< Video encoder for encoding the video frames
    VideoEncoderConfig*            m_pVideoEncoderConfig;           ///< Video encoder config
    int                            m_dumpPreviewFrames;             ///< Number of preview frames to dump
    int64_t                        m_exposureSetting;               ///< Exposure setting
    int32_t                        m_gainSetting;                   ///< Gain setting
    camera3_stream_t               m_streams[StreamTypeMax];        ///< Streams to be used for the camera request
    camera3_device_t*              m_pDevice;                       ///< HAL3 device
    android::CameraMetadata        m_requestMetadata;               ///< Per request metadata
    BufferManager*                 m_pBufferManager[StreamTypeMax]; ///< Buffer manager per stream
    ThreadData                     m_requestThread;                 ///< Request thread private data
    ThreadData                     m_resultThread;                  ///< Result thread private data
    ThreadData                     m_tensorflowliteThread;          ///< Tensorflow lite thread private data
    uint32_t                       m_nextResultFrameNumber;         ///< Next expected frame from the camera module
    TcpServer*                     m_pTcpServer;                    ///< TCP Server for streaming image
    TFliteThreadPrivateData        m_pTFliteThreadPrivateData;      ///< Tensorflow lite thread private data
    char*                          m_pIPAddress;                    ///< IP address to stream the RGB stream to
    char*                          m_pDnnModelFile;                 ///< Dnn Model filename
    char*                          m_pLabelsFile;                   ///< DNN Model labels filename
    bool                           m_isStreamingEnabled;            ///< Is streaming enabled
};

//------------------------------------------------------------------------------------------------------------------------------
// Main interface class to manage all the cameras
//------------------------------------------------------------------------------------------------------------------------------
class CameraHAL3
{
public:
    CameraHAL3();
    ~CameraHAL3() { }
    // Perform any one time initialization
    int Initialize();
    // Start a camera with the requested parameters
    int Start(int           cameraid,
              int           width,
              int           height,
              PreviewFormat format,
              CameraMode    mode,
              int           dumpPreviewFrames,
              char*         pIPAddress,
              char*         pDnnModelFile,
              char*         pLabelsFile);
    // Stop the opened camera and close it
    void Stop();

private:
    // Callback to indicate device status change
    static void CameraDeviceStatusChange(const struct camera_module_callbacks* callbacks, int camera_id, int new_status)
    {
    }
    // Callback to indicate torch mode status change
    static void TorchModeStatusChange(const struct camera_module_callbacks* callbacks, const char* camera_id, int new_status)
    {
    }

    // Load the camera module to start communicating with it
    int LoadCameraModule();

    static const uint32_t MaxCameras = 4;

    camera_module_t*        m_pCameraModule;                ///< Camera module
    camera_module_callbacks m_moduleCallbacks;              ///< Camera module callbacks
    PerCameraMgr*           m_pPerCameraMgr[MaxCameras];    ///< Each instance manages one camera
    int32_t                 m_numCameras;                   ///< Number of cameras detected
    camera_info             m_cameraInfo[MaxCameras];       ///< Per camera info
};

#endif // HELLO_HAL3_CAMERA
