/*==============================================================================
Copyright 2017 The TensorFlow Authors. All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Copyright 2020 Modified by ModalAI to run the object detection model on live camera frames
==============================================================================*/

#include <getopt.h>     // NOLINT(build/include_order)
#include <sys/time.h>   // NOLINT(build/include_order)
#include <sys/types.h>  // NOLINT(build/include_order)
#include <sys/resource.h>
#include <sys/syscall.h>
#include <cstdarg>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <vector>

#include "memory.h"
#include "nnapi_delegate.h"
#include "bitmap_helpers.h"
#include "optional_debug_tools.h"
#include "utils.h"
#include "tflite.h"
#include "hello_hal3_camera.h"
#include <opencv2/opencv.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include "tcp_utils.hpp"
#include "buffer_manager.h"

#define LOG(x) std::cout

void* ThreadTensorflowLite(void* data);

namespace tflite
{
namespace label_image
{

template<typename T>
T* TensorData(TfLiteTensor* tensor, int batch_index);

// -----------------------------------------------------------------------------------------------------------------------------
// Gets the float* tensor data pointer
// -----------------------------------------------------------------------------------------------------------------------------
template<>
float* TensorData(TfLiteTensor* tensor, int batch_index)
{
    int nelems = 1;

    for (int i = 1; i < tensor->dims->size; i++)
    {
        nelems *= tensor->dims->data[i];
    }

    switch (tensor->type)
    {
        case kTfLiteFloat32:
            return tensor->data.f + nelems * batch_index;
        default:
            LOG(FATAL) << "Should not reach here!";
    }

    return nullptr;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Gets the uint8_t* tensor data pointer
// -----------------------------------------------------------------------------------------------------------------------------
template<>
uint8_t* TensorData(TfLiteTensor* tensor, int batch_index)
{
    int nelems = 1;

    for (int i = 1; i < tensor->dims->size; i++)
    {
        nelems *= tensor->dims->data[i];
    }

    switch (tensor->type)
    {
        case kTfLiteUInt8:
            return tensor->data.uint8 + nelems * batch_index;
        default:
            LOG(FATAL) << "Should not reach here!";
    }

    return nullptr;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Gets the time in microsecs
// -----------------------------------------------------------------------------------------------------------------------------
double get_us(struct timeval t)
{
    return (t.tv_sec * 1000000 + t.tv_usec);
}

using TfLiteDelegatePtr    = tflite::Interpreter::TfLiteDelegatePtr;
using TfLiteDelegatePtrMap = std::map<std::string, TfLiteDelegatePtr>;

// -----------------------------------------------------------------------------------------------------------------------------
// Creates a GPU delegate
// -----------------------------------------------------------------------------------------------------------------------------
TfLiteDelegatePtr CreateGPUDelegate(Settings* s)
{
#if defined(__ANDROID__)
    TfLiteGpuDelegateOptionsV2 gpu_opts = TfLiteGpuDelegateOptionsV2Default();

    gpu_opts.inference_preference = TFLITE_GPU_INFERENCE_PREFERENCE_SUSTAINED_SPEED;
    gpu_opts.inference_priority1  = s->allow_fp16 ? TFLITE_GPU_INFERENCE_PRIORITY_MIN_LATENCY
                                    : TFLITE_GPU_INFERENCE_PRIORITY_MAX_PRECISION;

    return evaluation::CreateGPUDelegate(&gpu_opts);

#else
    return evaluation::CreateGPUDelegate(s->model);
#endif
}

// -----------------------------------------------------------------------------------------------------------------------------
// Gets all the available delegates
// -----------------------------------------------------------------------------------------------------------------------------
TfLiteDelegatePtrMap GetDelegates(Settings* s)
{
    TfLiteDelegatePtrMap delegates;

    if (s->gl_backend)
    {
        auto delegate = CreateGPUDelegate(s);

        if (!delegate)
        {
            LOG(INFO) << "GPU acceleration is unsupported on this platform.";
        } 
        else 
        {
            LOG(INFO) << "GPU acceleration is SUPPORTED on this platform\n";
            delegates.emplace("GPU", std::move(delegate));
        }
    }

    if (s->accel) 
    {
        auto delegate = evaluation::CreateNNAPIDelegate();

        if (!delegate)
        {
            LOG(INFO) << "NNAPI acceleration is unsupported on this platform.";
        } 
        else
        {
            delegates.emplace("NNAPI", evaluation::CreateNNAPIDelegate());
        }
    }

    return delegates;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Takes a file name, and loads a list of labels from it, one per line, and returns a vector of the strings. It pads with empty
// strings so the length of the result is a multiple of 16, because our model expects that.
// -----------------------------------------------------------------------------------------------------------------------------
TfLiteStatus ReadLabelsFile(const string& file_name,
                            std::vector<string>* result,
                            size_t* found_label_count)
{
    std::ifstream file(file_name);

    if (!file)
    {
        LOG(FATAL) << "Labels file " << file_name << " not found\n";
        return kTfLiteError;
    }
    result->clear();
    string line;

    while (std::getline(file, line))
    {
        result->push_back(line);
    }
    
    *found_label_count = result->size();
    const int padding = 16;

    while (result->size() % padding)
    {
        result->emplace_back();
    }

    return kTfLiteOk;
}

// -----------------------------------------------------------------------------------------------------------------------------
// This function rotates a NV21 image
// -----------------------------------------------------------------------------------------------------------------------------
void RotateNV21(uint8_t* pRotatedImage,
                uint8_t* pInputImage,
                int      width,
                int      height,
                int      rotation)
{
    if (rotation == 0)
    {
        return;
    }

    if (rotation % 90 != 0 || rotation < 0 || rotation > 270)
    {
        LOG(INFO) << "Rotation of only 90, 180 and 270 degrees is supported";
    }

    int  frameSize = width * height;
    bool swap      = rotation % 180 != 0;
    bool xflip     = rotation % 270 != 0;
    bool yflip     = rotation >= 180;

    for (int j = 0; j < height; j++)
    {
        for (int i = 0; i < width; i++)
        {
            int yIn = j * width + i;
            int uIn = frameSize + (j >> 1) * width + (i & ~1);
            int vIn = uIn       + 1;

            int wOut     = swap  ? height              : width;
            int hOut     = swap  ? width               : height;
            int iSwapped = swap  ? j                   : i;
            int jSwapped = swap  ? i                   : j;
            int iOut     = xflip ? wOut - iSwapped - 1 : iSwapped;
            int jOut     = yflip ? hOut - jSwapped - 1 : jSwapped;

            int yOut = jOut * wOut + iOut;
            int uOut = frameSize + (jOut >> 1) * wOut + (iOut & ~1);
            int vOut = uOut + 1;

            pRotatedImage[yOut] = (uint8_t)(0xff & pInputImage[yIn]);
            pRotatedImage[uOut] = (uint8_t)(0xff & pInputImage[uIn]);
            pRotatedImage[vOut] = (uint8_t)(0xff & pInputImage[vIn]);
        }
    }
}

// -----------------------------------------------------------------------------------------------------------------------------
// This function runs the object detection model on every live camera frame
// -----------------------------------------------------------------------------------------------------------------------------
void TfliteObjectDetect(void* pData)
{
    int wanted_height;
    int wanted_width;
    int wanted_channels;
    std::unique_ptr<tflite::FlatBufferModel> model;
    std::unique_ptr<tflite::Interpreter> interpreter;
    tflite::ops::builtin::BuiltinOpResolver resolver;
    tflite::label_image::Settings* s = NULL;
    std::vector<uint8_t> in(3840 * 2160 * 3);
    cv::Mat rgbImage = cv::Mat();
    cv::Mat resizedImage = cv::Mat();
    ThreadData* pThreadData = (ThreadData*)pData;
    TFliteThreadPrivateData* pPvtData = (TFliteThreadPrivateData*)pThreadData->pPrivate;
    TcpServer* pTcpServer = pPvtData->pTcpServer;
    uint8_t* pRotatedYuv = new uint8_t[3840 * 2160 * 3];
    uint8_t* pTempYuv = new uint8_t[1920 * 1080 * 2];

    s = new tflite::label_image::Settings;

    s->model_name                   = pPvtData->pDnnModelFile;
    s->labels_file_name             = pPvtData->pLabelsFile;
    s->input_bmp_name               = "";
    s->gl_backend                   = 1;
    s->number_of_threads            = 0;
    s->allow_fp16                   = 1;
    s->input_mean                   = 127;
    s->accel                        = 0;
    s->old_accel                    = 0;
    s->max_profiling_buffer_entries = 0;
    s->profiling                    = 0;
    s->verbose                      = 0;
    s->number_of_warmup_runs        = 0;
    s->loop_count                   = 1;

    if (!s->model_name.c_str()) 
    {
        LOG(ERROR) << "no model file name\n";
        exit(-1);
    }

    model = tflite::FlatBufferModel::BuildFromFile(s->model_name.c_str());

    if (!model)
    {
        LOG(FATAL) << "\nFailed to mmap model " << s->model_name << "\n";
        exit(-1);
    }

    s->model = model.get();
    LOG(INFO) << "\nLoaded model " << s->model_name << "\n";
    model->error_reporter();
    LOG(INFO) << "Resolved reporter\n";

    tflite::InterpreterBuilder(*model, resolver)(&interpreter);

    if (!interpreter)
    {
        LOG(FATAL) << "Failed to construct interpreter\n";
        exit(-1);
    }

    interpreter->UseNNAPI(s->old_accel);
    interpreter->SetAllowFp16PrecisionForFp32(s->allow_fp16);

    if (s->verbose)
    {
        LOG(INFO) << "tensors size: " << interpreter->tensors_size() << "\n";
        LOG(INFO) << "nodes size: " << interpreter->nodes_size() << "\n";
        LOG(INFO) << "inputs: " << interpreter->inputs().size() << "\n";
        LOG(INFO) << "input(0) name: " << interpreter->GetInputName(0) << "\n";

        int t_size = interpreter->tensors_size();

        for (int i = 0; i < t_size; i++) 
        {
            if (interpreter->tensor(i)->name)
              LOG(INFO) << i << ": " << interpreter->tensor(i)->name << ", "
                        << interpreter->tensor(i)->bytes << ", "
                        << interpreter->tensor(i)->type << ", "
                        << interpreter->tensor(i)->params.scale << ", "
                        << interpreter->tensor(i)->params.zero_point << "\n";
        }
    }

    if (s->number_of_threads != -1)
    {
        interpreter->SetNumThreads(s->number_of_threads);
    }

    TfLiteDelegatePtrMap delegates_ = GetDelegates(s);

    for (const auto& delegate : delegates_)
    {
        if (interpreter->ModifyGraphWithDelegate(delegate.second.get()) != kTfLiteOk)
        {
            LOG(FATAL) << "Failed to apply " << delegate.first << " delegate\n";
            break;
        } 
        else
        {
            LOG(INFO) << "Applied " << delegate.first << " delegate ";
            break;
        }
    }

    if (interpreter->AllocateTensors() != kTfLiteOk)
    {
        LOG(FATAL) << "Failed to allocate tensors!";
    }

    TfLiteIntArray* dims = interpreter->tensor(interpreter->inputs()[0])->dims;

    wanted_height   = dims->data[1];
    wanted_width    = dims->data[2];
    wanted_channels = dims->data[3];
      
    // Set thread priority
    pid_t tid = syscall(SYS_gettid);
    int which = PRIO_PROCESS;
    int nice  = -10;

    struct timeval begin_time;
    struct timeval start_time, stop_time;
    struct timeval resize_start_time, resize_stop_time;
    struct timeval yuvrgb_start_time, yuvrgb_stop_time;
    uint32_t totalResizeTimemsecs = 0;
    uint32_t totalYuvRgbTimemsecs = 0;
    uint32_t totalGpuExecutionTimemsecs = 0;
    uint32_t numFrames = 0;

    gettimeofday(&begin_time, nullptr);

    setpriority(which, tid, nice);

    while (pThreadData->stop == false)
    {
        pthread_mutex_lock(&pThreadData->mutex);

        if (pThreadData->msgQueue.empty())
        {
            struct timespec tv;
            clock_gettime(CLOCK_MONOTONIC, &tv);
            tv.tv_sec += 1;

            // Go to a temporary small sleep waiting for a frame to arrive
            if(pthread_cond_timedwait(&pThreadData->cond, &pThreadData->mutex, &tv) != 0)
            {
                pthread_mutex_unlock(&pThreadData->mutex);
                continue;
            }
        }

        // Coming here means we have a frame to run through the DNN model
        numFrames++;

        TensorflowMessage* pTensorflowMessage = (TensorflowMessage*)pThreadData->msgQueue.front();
        BufferInfo*        pBufferInfo        = pTensorflowMessage->pBufferInfo;

        pThreadData->msgQueue.pop_front();
        pthread_mutex_unlock(&pThreadData->mutex);

        int image_width    = pBufferInfo->width;
        int image_height   = pBufferInfo->height;
        int image_channels = 3;
        int rotation       = 180;

        gettimeofday(&yuvrgb_start_time, nullptr);
        memcpy(pTempYuv, (uint8_t*)pBufferInfo->vaddr, image_width*image_height);
        memcpy(pTempYuv+(image_width*image_height), (uint8_t*)pBufferInfo->craddr, image_width*image_height/2);

        RotateNV21(pRotatedYuv, (uint8_t*)pTempYuv, image_width, image_height, rotation);
        cv::Mat yuv(image_height + image_height/2, image_width, CV_8UC1, (uchar*)pRotatedYuv);
        cv::cvtColor(yuv, rgbImage, CV_YUV2RGB_NV21);
        cv::resize(rgbImage, resizedImage, cv::Size(wanted_width, wanted_height), 0, 0, CV_INTER_LINEAR);
        gettimeofday(&yuvrgb_stop_time, nullptr);

        totalYuvRgbTimemsecs += (get_us(yuvrgb_stop_time) - get_us(yuvrgb_start_time)) / 1000;

        uint8_t*               pImageData = (uint8_t*)resizedImage.data;
        const std::vector<int> inputs     = interpreter->inputs();
        const std::vector<int> outputs    = interpreter->outputs();

        // Get input dimension from the input tensor metadata assuming one input only

        int input = interpreter->inputs()[0];

        if (s->verbose)
        {
            PrintInterpreterState(interpreter.get());
        }

        gettimeofday(&resize_start_time, nullptr);

        switch (interpreter->tensor(input)->type)
        {
            case kTfLiteFloat32:
              s->input_floating = true;
              resize<float>(interpreter->typed_tensor<float>(input), pImageData,
                            wanted_height, wanted_width, image_channels, wanted_height,
                            wanted_width, wanted_channels, s);
              break;

            case kTfLiteUInt8:
              resize<uint8_t>(interpreter->typed_tensor<uint8_t>(input), pImageData,
                              wanted_height, wanted_width, image_channels, wanted_height,
                              wanted_width, wanted_channels, s);
              break;

            default:
              LOG(FATAL) << "cannot handle input type "
                        << interpreter->tensor(input)->type << " yet";
              exit(-1);
        }

        gettimeofday(&resize_stop_time, nullptr);

        totalResizeTimemsecs += (get_us(resize_stop_time) - get_us(resize_start_time)) / 1000;

        gettimeofday(&start_time, nullptr);

        for (int i = 0; i < s->loop_count; i++)
        {
            if (interpreter->Invoke() != kTfLiteOk)
            {
                LOG(FATAL) << "Failed to invoke tflite!\n";
            }
        }

        gettimeofday(&stop_time, nullptr);
        LOG(INFO) << "GPU invoked \n";
        LOG(INFO) << "average GPU model execution time: "
                  << (get_us(stop_time) - get_us(start_time)) / (s->loop_count * 1000)
                  << " ms \n";

        totalGpuExecutionTimemsecs += (get_us(stop_time) - get_us(start_time)) / 1000;

        // https://www.tensorflow.org/lite/models/object_detection/overview#starter_model
        TfLiteTensor* output_locations    = interpreter->tensor(interpreter->outputs()[0]);
        TfLiteTensor* output_classes      = interpreter->tensor(interpreter->outputs()[1]);
        TfLiteTensor* output_scores       = interpreter->tensor(interpreter->outputs()[2]);
        TfLiteTensor* output_detections   = interpreter->tensor(interpreter->outputs()[3]);
        const float*  detected_locations  = TensorData<float>(output_locations, 0);        
        const float*  detected_classes    = TensorData<float>(output_classes, 0);
        const float*  detected_scores     = TensorData<float>(output_scores, 0);        
        const int     detected_numclasses = (int)(*TensorData<float>(output_detections, 0));

        std::vector<string> labels;
        size_t label_count;

        if (ReadLabelsFile(s->labels_file_name, &labels, &label_count) != kTfLiteOk)
        {
            exit(-1);
        }

        LOG(INFO) << "Frame: " << pTensorflowMessage->frameNumber << ".... Detected Num Classes is: " << detected_numclasses << "\n";

        for (int i = 0; i < detected_numclasses; i++)
        {
            const float score  = detected_scores[i];
            const int   top    = detected_locations[4 * i + 0] * image_height;
            const int   left   = detected_locations[4 * i + 1] * image_width;
            const int   bottom = detected_locations[4 * i + 2] * image_height;
            const int   right  = detected_locations[4 * i + 3] * image_width;

            // Check for object detection confidence of 60% or more
            if (score > 0.6f)
            {
                LOG(INFO) << score * 100.0 << "\t" << " Class id:  " << labels[detected_classes[i]]
                          << "\t" << "[ " << left << ", " << top << ", " << right-left << ", " << bottom-top << " ]" << "\n";              

                int height = bottom - top;
                int width  = right - left;

                cv::Rect rect(left, top, width, height);
                cv::Point pt(left, top);

                cv::rectangle(rgbImage, rect, cv::Scalar(0, 200, 0), 7);
                cv::putText(rgbImage, labels[detected_classes[i]], pt, cv::FONT_HERSHEY_SIMPLEX, 0.8, cv::Scalar(255, 0, 0), 2);

#if FRAME_DUMP
                char filename[128];
                {
                    sprintf(filename, "frame_%d.bmp", pTensorflowMessage->frameNumber);
                    cv::imwrite(filename, rgbImage);
                }
#endif // FRAME_DUMP                
            }
        }

        LOG(INFO) << "\n\n";

        // Stream the RGB image over the tcp socket
        if (pTcpServer != NULL)
        {
            uint8_t opts[4] = {0, 0, 0, 0};
            uint64_t timestampNsecs = (stop_time.tv_sec*1e9) + (stop_time.tv_usec*1e3);
            cv::Mat gray(image_height, image_width, CV_8UC1);

            cv::cvtColor(rgbImage, gray, CV_BGR2GRAY);

            pTcpServer->send_message(rgbImage.data,
                                     image_width*image_height*3,
                                     2,
                                     image_width,
                                     image_height,
                                     &opts[0],
                                     pTensorflowMessage->frameNumber,
                                     timestampNsecs);
        }
    }

#if STATS_DUMP
    struct timeval end_time;
    gettimeofday(&end_time, nullptr);
    LOG(INFO) << "\n\nAverage execution time per frame: ";
    LOG(INFO) << ((get_us(end_time) - get_us(begin_time)) / 1000) / numFrames << " ms \n";
    LOG(INFO) << "Average resize time per frame: " << totalResizeTimemsecs / numFrames << "\n";
    LOG(INFO) << "Average yuv-->rgb time per frame: " << totalYuvRgbTimemsecs / numFrames << "\n";
    LOG(INFO) << "\n\n ==== Average GPU model execution time per frame: " << totalGpuExecutionTimemsecs / numFrames << " msecs\n";
#endif // STATS_DUMP

    while (!pThreadData->msgQueue.empty())
    {
        TensorflowMessage* pTensorflowMessage = (TensorflowMessage*)pThreadData->msgQueue.front();
        pThreadData->msgQueue.pop_front();
        delete pTensorflowMessage;
    }

    if (s != NULL)
    {
        delete s;
    }
}

}  // namespace label_image
}  // namespace tflite

// -----------------------------------------------------------------------------------------------------------------------------
// This thread runs the tensorflow model
// -----------------------------------------------------------------------------------------------------------------------------
void* ThreadTensorflowLite(void* pData)
{
    tflite::label_image::TfliteObjectDetect(pData);
    return NULL;
}