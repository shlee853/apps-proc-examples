/*******************************************************************************************************************************
 *
 * Copyright (c) 2019 ModalAI, Inc.
 *
 ******************************************************************************************************************************/
#include <sys/resource.h>
#include <sys/syscall.h>
#include <iostream>
#include "buffer_manager.h"
#include "hello_hal3_camera.h"
#include "hello_hal3_video_encoder.h"
#include "tcp_utils.hpp"
#include "tflite.h"

// Main thread functions for request and result processing
void* ThreadPostProcessResult(void* data);
void* ThreadIssueCaptureRequests(void* data);
// Tensorflow thread
extern void* ThreadTensorflowLite(void* data);

// -----------------------------------------------------------------------------------------------------------------------------
// Filled in when the camera module sends result image buffers to us. This gets passed to the capture result handling threads's
// message queue
// -----------------------------------------------------------------------------------------------------------------------------
struct CaptureResultFrameData
{
    // Either preview or video or both may be valid
    BufferInfo* pPreviewBufferInfo;     ///< Preview buffer information
    BufferInfo* pVideoBufferInfo;       ///< Video buffer information
    int64_t     timestampNsecs;         ///< Timestamp of the buffer(s) in nano secs
    int         frameNumber;            ///< Frame number associated with the image buffers
};

// -----------------------------------------------------------------------------------------------------------------------------
// Constructor
// -----------------------------------------------------------------------------------------------------------------------------
PerCameraMgr::PerCameraMgr()
{
    m_pDevice        = NULL;
    m_previewFormat = 0;
    m_width         = 0;
    m_height        = 0;
    m_pTcpServer    = NULL;

    for (uint32_t i = 0; i < StreamTypeMax; i++)
    {
        m_pBufferManager[i] = NULL;
    }

    m_requestThread.pCameraMgr = this;
    m_requestThread.stop       = false;
    m_requestThread.pPrivate   = NULL;
    m_requestThread.msgQueue.clear();

    m_resultThread.pCameraMgr = this;
    m_resultThread.stop       = false;
    m_resultThread.pPrivate   = NULL;
    m_resultThread.msgQueue.clear();
    m_resultThread.lastResultFrameNumber = -1;

    m_pVideoEncoder       = NULL;
    m_pVideoEncoderConfig = NULL;

    m_nextResultFrameNumber = 0;

    m_tensorflowliteThread.pCameraMgr = this;
    m_tensorflowliteThread.stop       = false;
    m_tensorflowliteThread.pPrivate   = &m_pTFliteThreadPrivateData;
    m_tensorflowliteThread.msgQueue.clear();

    m_pTFliteThreadPrivateData.pVideoEncoder = NULL;

    m_pTcpServer = NULL;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Performs any one time initialization. This function should only be called once.
// -----------------------------------------------------------------------------------------------------------------------------
int PerCameraMgr::Initialize(PerCameraInitData* pInitData)  ///< Number of preview frames to dump
{
    int status = 0;

    m_pCameraModule                   = pInitData->pCameraModule;
    m_pCameraInfo                     = pInitData->pCameraInfo;
    m_cameraId                        = pInitData->cameraid;
    m_cameraMode                      = pInitData->mode;
    m_width                           = pInitData->width;
    m_height                          = pInitData->height;
    m_faceDetect                      = pInitData->faceDetect;
    m_dnn                             = pInitData->dnn;
    m_armcl                           = pInitData->armcl;
    m_pVideoFilename                  = pInitData->pVideoFilename;
    m_pVideoFilehandle                = 0;
    m_dumpPreviewFrames               = pInitData->dumpPreviewFrames;
    m_pIPAddress                      = pInitData->pIPAddress;
    m_pDnnModelFile                   = pInitData->pDnnModelFile;
    m_pLabelsFile                     = pInitData->pLabelsFile;
    m_cameraCallbacks.cameraCallbacks = {CameraModuleCaptureResult, CameraModuleNotify};
    m_cameraCallbacks.pPrivate        = this;

    m_pTFliteThreadPrivateData.pDnnModelFile = m_pDnnModelFile;
    m_pTFliteThreadPrivateData.pLabelsFile   = m_pLabelsFile;

    if (m_pIPAddress[0] == '0')
    {
        m_isStreamingEnabled = false;  
        m_pTcpServer         = NULL;     
    }
    else
    {
         m_isStreamingEnabled = true;
         m_pTcpServer         = new TcpServer;
    }

    m_pTFliteThreadPrivateData.pTcpServer = m_pTcpServer;

    if (pInitData->format == PreviewFormatNV21)
    {
        m_previewFormat = HAL_PIXEL_FORMAT_YCbCr_420_888;
    }
    else if (pInitData->format == PreviewFormatRAW8)
    {
        m_previewFormat = HAL_PIXEL_FORMAT_RAW10;
        m_faceDetect    = 0;
        m_dnn           = 0;
        m_armcl         = 0;
    }
    else if (pInitData->format == PreviewFormatBLOB)
    {
        m_previewFormat = HAL_PIXEL_FORMAT_BLOB;
        m_faceDetect    = 0;
        m_dnn           = 0;
        m_armcl         = 0;
    }

    // Check if the stream configuration is supported by the camera or not. If cameraid doesnt support the stream configuration
    // we just exit. The stream configuration is checked into the static metadata associated with every camera.
    if (IsStreamConfigSupported(m_width, m_height, m_previewFormat) == true)
    {
        if (m_cameraMode == CameraModeVideo)
        {
            m_pVideoEncoder       = new VideoEncoder;
            m_pVideoEncoderConfig = new VideoEncoderConfig;

            memset(m_pVideoEncoderConfig, 0, sizeof(VideoEncoderConfig));

            if (m_pVideoFilename != NULL)
            {
                m_pVideoFilehandle = fopen(m_pVideoFilename, "w+");

                if (m_pVideoFilehandle == NULL)
                {
                    status = -EINVAL;
                    printf("\nCannot open video file to write to!");
                }
                else
                {
                    m_pVideoEncoderConfig->width             = m_width;
                    m_pVideoEncoderConfig->height            = m_height;
                    m_pVideoEncoderConfig->format            = VideoImageFormat;
                    m_pVideoEncoderConfig->isH265            = true;
                    m_pVideoEncoderConfig->frameRate         = FrameRate[1];
                    m_pVideoEncoderConfig->isBitRateConstant = true;
                    m_pVideoEncoderConfig->pVideoFileHandle  = m_pVideoFilehandle;

                    status = m_pVideoEncoder->Initialize(m_pVideoEncoderConfig);

                    if (status != 0)
                    {
                        printf("\nHELLOHAL3VIDEO-ERROR: Video Encoder Initialization failed!");
                    }
                    else
                    {
                        printf("\nHELLOHAL3VIDEO-INFO: Video Encoder Initialization PASSED!");
                    }
                    
                }
            }
            else
            {
                printf("\nVideo mode requested with an empty filename!");
                status = -EINVAL;
            }

            m_pTFliteThreadPrivateData.pVideoEncoder = m_pVideoEncoder;
        }
    }
    else
    {
        status = -EINVAL;
    }

    char cameraName[20] = {0};
    sprintf(cameraName, "%d", m_cameraId);

    if (status == 0)
    {
        status = m_pCameraModule->common.methods->open(&m_pCameraModule->common, cameraName, (hw_device_t**)(&m_pDevice));

        if (status != 0)
        {
            printf("\nOpen camera %s failed!", cameraName);
        }
    }

    if (status == 0)
    {
        status = m_pDevice->ops->initialize(m_pDevice, (camera3_callback_ops*)&m_cameraCallbacks);

        if (status != 0)
        {
            printf("\nInitialize camera %s failed!", cameraName);
        }
    }

    if (status == 0)
    {
        // This calls into the camera module and checks if it supports the stream configuration. If it doesnt then we have to
        // bail out.
        status = ConfigureStreams();
    }

    // Since ConfigureStreams is successful lets allocate the buffer memory since we are definitely going to start processing
    // camera frames now
    if (status == 0)
    {
        status = AllocateStreamBuffers();
    }

    if (status == 0)
    {
        // This is the default metadata i.e. camera settings per request. The camera module passes us the best set of baseline
        // settings. We can modify any setting, for any frame or for every frame, as we see fit.
        ConstructDefaultRequestSettings();

        if (IsStreamingEnabled() == true)
        {
            int portNum = 5556;

            m_pTcpServer->create_socket(m_pIPAddress, portNum);
            m_pTcpServer->bind_socket();
            m_pTcpServer->connect_client();
        }
    }

    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Create the streams that we will use to communicate with the camera module
// -----------------------------------------------------------------------------------------------------------------------------
int PerCameraMgr::ConfigureStreams()
{
    int status = 0;
    camera3_stream_configuration_t streamConfig = { 0 };

    m_streams[StreamTypePreview].stream_type = CAMERA3_STREAM_OUTPUT;
    m_streams[StreamTypePreview].width       = m_width;
    m_streams[StreamTypePreview].height      = m_height;
    m_streams[StreamTypePreview].format      = m_previewFormat;
    m_streams[StreamTypePreview].data_space  = HAL_DATASPACE_UNKNOWN;
#ifdef USE_GRALLOC1
    m_streams[StreamTypePreview].usage = GRALLOC1_CONSUMER_USAGE_HWCOMPOSER | GRALLOC1_CONSUMER_USAGE_GPU_TEXTURE;
#else
    m_streams[StreamTypePreview].usage = GRALLOC_USAGE_HW_COMPOSER | GRALLOC_USAGE_HW_TEXTURE;
#endif
    m_streams[StreamTypePreview].rotation    = 0;
    m_streams[StreamTypePreview].max_buffers = MaxPreviewBuffers;
    m_streams[StreamTypePreview].priv        = 0;

    m_streams[StreamTypeVideo].stream_type   = CAMERA3_STREAM_OUTPUT;
    m_streams[StreamTypeVideo].width         = m_width;
    m_streams[StreamTypeVideo].height        = m_height;
    m_streams[StreamTypeVideo].format        = VideoImageFormat;
    m_streams[StreamTypeVideo].data_space    = HAL_DATASPACE_BT709;
    m_streams[StreamTypeVideo].usage         = GRALLOC_USAGE_HW_VIDEO_ENCODER;
    m_streams[StreamTypeVideo].rotation      = 0;
    m_streams[StreamTypeVideo].max_buffers   = MaxVideoBuffers;
    m_streams[StreamTypeVideo].priv          = 0;

    if (m_cameraMode == CameraModePreview)
    {
        streamConfig.num_streams    = 1;
        streamConfig.operation_mode = QCAMERA3_VENDOR_STREAM_CONFIGURATION_RAW_ONLY_MODE;
    }
    else if (m_cameraMode == CameraModeVideo)
    {
        streamConfig.num_streams    = 2; // Implies preview + video
        streamConfig.operation_mode = CAMERA3_STREAM_CONFIGURATION_NORMAL_MODE;
    }
    
    camera3_stream_t* pStreams[] = { &m_streams[0], &m_streams[1] };
    streamConfig.streams = &pStreams[0];

    // Call into the camera module to check for support of the required stream config i.e. the required usecase
    status = m_pDevice->ops->configure_streams(m_pDevice, &streamConfig);

    if (status != 0)
    {
        printf("\nHELLOCAMERA-ERROR: Configure streams failed!");
    }

    m_streams[StreamTypePreview].max_buffers = MaxPreviewBuffers;
    m_streams[StreamTypeVideo].max_buffers   = MaxVideoBuffers;

    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Allocate the buffers required per stream. Each stream will have its own BufferManager to manage buffers for that stream
// -----------------------------------------------------------------------------------------------------------------------------
int PerCameraMgr::AllocateStreamBuffers()
{
    int status = 0;

    if (m_cameraMode == CameraModePreview)
    {
        m_pBufferManager[StreamTypePreview] = new BufferManager;

        status = m_pBufferManager[StreamTypePreview]->Initialize(m_streams[StreamTypePreview].max_buffers);

        if (status == 0)
        {
            status = m_pBufferManager[StreamTypePreview]->AllocateBuffers(m_streams[StreamTypePreview].width,
                                                                          m_streams[StreamTypePreview].height,
                                                                          m_streams[StreamTypePreview].format,
                                                                          m_streams[StreamTypePreview].usage,
                                                                          m_streams[StreamTypePreview].usage);
        }
    }
    else if (m_cameraMode == CameraModeVideo)
    {
        m_pBufferManager[StreamTypePreview] = new BufferManager;
        m_pBufferManager[StreamTypeVideo]   = new BufferManager;

        // Allocate preview buffers
        status =  m_pBufferManager[StreamTypePreview]->Initialize(m_streams[StreamTypePreview].max_buffers);

        if (status == 0)
        {
            status = m_pBufferManager[StreamTypePreview]->AllocateBuffers(m_streams[StreamTypePreview].width,
                                                                          m_streams[StreamTypePreview].height,
                                                                          m_streams[StreamTypePreview].format,
                                                                          m_streams[StreamTypePreview].usage,
                                                                          m_streams[StreamTypePreview].usage);
        }

        // Allocate video buffers
        if (status == 0)
        {
            status = m_pBufferManager[StreamTypeVideo]->Initialize(m_streams[StreamTypeVideo].max_buffers);
        }

        if (status == 0)
        {
            status = m_pBufferManager[StreamTypeVideo]->AllocateBuffers(m_streams[StreamTypeVideo].width,
                                                                        m_streams[StreamTypeVideo].height,
                                                                        m_streams[StreamTypeVideo].format,
                                                                        m_streams[StreamTypeVideo].usage,
                                                                        m_streams[StreamTypeVideo].usage);
        }
    }

    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Construct default camera settings that will be passed to the camera module to be used for capturing the frames
// -----------------------------------------------------------------------------------------------------------------------------
void PerCameraMgr::ConstructDefaultRequestSettings()
{
    camera3_request_template_t type = CAMERA3_TEMPLATE_PREVIEW;

    if (m_cameraMode == CameraModePreview)
    {
        type = CAMERA3_TEMPLATE_PREVIEW;
    }
    else if (m_cameraMode == CameraModeVideo)
    {
        type = CAMERA3_TEMPLATE_VIDEO_RECORD;
    }

    // Get the default baseline settings
    camera_metadata_t* pDefaultMetadata = (camera_metadata_t *)m_pDevice->ops->construct_default_request_settings(m_pDevice,
                                                                                                                  type);

    // Modify all the settings that we want to
    m_requestMetadata = clone_camera_metadata(pDefaultMetadata);
    m_requestMetadata.update(ANDROID_CONTROL_AE_TARGET_FPS_RANGE, &FrameRate[0], 2);

    uint8_t antibanding = ANDROID_CONTROL_AE_ANTIBANDING_MODE_AUTO;
    m_requestMetadata.update(ANDROID_CONTROL_AE_ANTIBANDING_MODE,&(antibanding),sizeof(antibanding));

    uint8_t afmode = ANDROID_CONTROL_AF_MODE_CONTINUOUS_VIDEO;
    m_requestMetadata.update(ANDROID_CONTROL_AF_MODE, &(afmode), 1);

    uint8_t reqFaceDetectMode =  (uint8_t)ANDROID_STATISTICS_FACE_DETECT_MODE_OFF;
    m_requestMetadata.update(ANDROID_STATISTICS_FACE_DETECT_MODE, &reqFaceDetectMode, 1);

    uint8_t aeMode         = 1;
    // These are some (psuedo random) initial default values
    int     gainTarget     = 1000;
    int64_t exposureTarget = 8259763; // nsecs exposure time
    m_requestMetadata.update(ANDROID_CONTROL_AE_MODE, &aeMode, 1);
    m_requestMetadata.update(ANDROID_SENSOR_SENSITIVITY, &gainTarget, 1);
    m_requestMetadata.update(ANDROID_SENSOR_EXPOSURE_TIME, &exposureTarget, 1);  // Exposure time in nsecs

    // This is from the PMD RoyaleSDK that processes the raw image from the TOF camera and returns images of these types.
    // The camera module invokes the PMD libraries to process the raw data from the TOF camera into these image types
    typedef enum
    {
        LISTENER_NONE               = 0x0,
        LISTENER_DEPTH_DATA         = 0x1,
        LISTENER_SPARSE_POINT_CLOUD = 0x2,
        LISTENER_DEPTH_IMAGE        = 0x4,  // 16 bit
        LISTENER_IR_IMAGE           = 0x8   // 8 bit
    } RoyaleListenerType;

    uint8_t data = LISTENER_IR_IMAGE;

    m_requestMetadata.update(ANDROID_TOF_DATA_OUTPUT, &data, 1);
}

// -----------------------------------------------------------------------------------------------------------------------------
// This function opens the camera and starts sending the capture requests
// -----------------------------------------------------------------------------------------------------------------------------
int PerCameraMgr::Start()
{
    int status = 0;

    m_requestThread.pDevice = m_pDevice;
    m_resultThread.pDevice  = m_pDevice;

    if (m_cameraMode == CameraModeVideo)
    {
        m_pVideoEncoder->Start();
    }

    pthread_condattr_t attr;
    pthread_condattr_init(&attr);
    pthread_condattr_setclock(&attr, CLOCK_MONOTONIC);
    pthread_mutex_init(&m_requestThread.mutex, NULL);
    pthread_mutex_init(&m_resultThread.mutex, NULL);
    pthread_mutex_init(&m_tensorflowliteThread.mutex, NULL);
    pthread_cond_init(&m_requestThread.cond, &attr);
    pthread_cond_init(&m_resultThread.cond, &attr);
    pthread_cond_init(&m_tensorflowliteThread.cond, &attr);
    pthread_condattr_destroy(&attr);

    // Start the thread that will process the camera capture result. This thread wont exit till it consumes all expected
    // output buffers from the camera module
    pthread_attr_t resultAttr;
    pthread_attr_init(&resultAttr);
    pthread_attr_setdetachstate(&resultAttr, PTHREAD_CREATE_JOINABLE);
    pthread_create(&(m_resultThread.thread), &resultAttr, ThreadPostProcessResult, &m_resultThread);
    pthread_attr_destroy(&resultAttr);

    // Start the thread that will send the camera capture request. This thread wont stop issuing requests to the camera
    // module until we terminate the program with Ctrl+C
    pthread_attr_t requestAttr;
    pthread_attr_init(&requestAttr);
    pthread_attr_setdetachstate(&requestAttr, PTHREAD_CREATE_JOINABLE);
    pthread_create(&(m_requestThread.thread), &requestAttr, ThreadIssueCaptureRequests, &m_requestThread);
    pthread_attr_destroy(&requestAttr);

    // Start the thread that will run the tensorflow lite model to detect objects in the camera frames. This thread wont
    // stop issuing requests to the camera module until we terminate the program with Ctrl+C
    pthread_attr_t tfliteAttr;
    pthread_attr_init(&tfliteAttr);
    pthread_attr_setdetachstate(&tfliteAttr, PTHREAD_CREATE_JOINABLE);
    pthread_create(&(m_tensorflowliteThread.thread), &tfliteAttr, ThreadTensorflowLite, &m_tensorflowliteThread);
    pthread_attr_destroy(&tfliteAttr);    

    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// This function stops the camera and does all necessary clean up
// -----------------------------------------------------------------------------------------------------------------------------
void PerCameraMgr::Stop()
{
    // The result thread will stop when the result of the last frame is received
    m_requestThread.stop = true;

    pthread_join(m_requestThread.thread, NULL);
    pthread_cond_signal(&m_requestThread.cond);
    pthread_mutex_unlock(&m_requestThread.mutex);
    pthread_mutex_destroy(&m_requestThread.mutex);
    pthread_cond_destroy(&m_requestThread.cond);

    pthread_join(m_resultThread.thread, NULL);
    pthread_cond_signal(&m_resultThread.cond);
    pthread_mutex_unlock(&m_resultThread.mutex);
    pthread_mutex_destroy(&m_resultThread.mutex);
    pthread_cond_destroy(&m_resultThread.cond);

    m_tensorflowliteThread.stop = true;
    pthread_join(m_tensorflowliteThread.thread, NULL);
    pthread_cond_signal(&m_tensorflowliteThread.cond);
    pthread_mutex_unlock(&m_tensorflowliteThread.mutex);
    pthread_mutex_destroy(&m_tensorflowliteThread.mutex);
    pthread_cond_destroy(&m_tensorflowliteThread.cond);

    if (m_pVideoEncoder != NULL)
    {
        // This call wont complete till the last encoded video frame is received from the video encoder and written to the file
        m_pVideoEncoder->Stop();
        delete m_pVideoEncoder;
    }

    if (m_pVideoEncoderConfig != NULL)
    {
        delete m_pVideoEncoderConfig;
    }

    if (m_pDevice != NULL)
    {
        m_pDevice->common.close(&m_pDevice->common);

        if (m_pVideoFilehandle > 0)
        {
            fclose(m_pVideoFilehandle);
        }

        m_pDevice = NULL;
    }

    for (uint32_t i = 0; i < StreamTypeMax; i++)
    {
        if (m_pBufferManager[i] != NULL)
        {
            delete m_pBufferManager[i];
            m_pBufferManager[i] = NULL;
        }
    }

    if (m_pTcpServer != NULL)
    {
        delete m_pTcpServer;
    }
}

// -----------------------------------------------------------------------------------------------------------------------------
// Check if the stream resolution, format is supported in the camera static characteristics
// -----------------------------------------------------------------------------------------------------------------------------
bool PerCameraMgr::IsStreamConfigSupported(int width, int height, int format)
{
    bool isStreamSupported = false;

    if (m_pCameraInfo != NULL)
    {
        camera_metadata_t* pStaticMetadata = (camera_metadata_t *)m_pCameraInfo->static_camera_characteristics;
        camera_metadata_ro_entry entry;

        // Get the list of all stream resolutions supported and then go through each one of them looking for a match
        int status = find_camera_metadata_ro_entry(pStaticMetadata, ANDROID_SCALER_AVAILABLE_STREAM_CONFIGURATIONS, &entry);

        if ((0 == status) && (0 == (entry.count % 4))) 
        {
            for (size_t i = 0; i < entry.count; i+=4) 
            {
                if (ANDROID_SCALER_AVAILABLE_STREAM_CONFIGURATIONS_OUTPUT == entry.data.i32[i + 3])
                {
                    if ((format == entry.data.i32[i])   &&
                        (width  == entry.data.i32[i+1]) &&
                        (height == entry.data.i32[i+2])) 
                    {
                        isStreamSupported = true;
                        break;
                    }
                }
            }
        }
    }

    if (isStreamSupported == false)
    {
        printf("\nCamera Width: %d, Height: %d, Format: %d not supported!", width, height, format);
    }

    return isStreamSupported;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Function that will process one capture result sent from the camera module. Remember this function is operating in the camera
// module thread context. So we do the bare minimum work that we need to do and return control back to the camera module. The
// bare minimum we do is to dispatch the work to another worker thread who consumes the image buffers passed to it from here.
// Our result worker thread is "ThreadPostProcessResult(..)"
// -----------------------------------------------------------------------------------------------------------------------------
void PerCameraMgr::ProcessOneCaptureResult(const camera3_capture_result* pHalResult)
{
    if (pHalResult->num_output_buffers > 0)
    {
        CaptureResultFrameData* pCaptureResultData = new CaptureResultFrameData;

        // The camera frames is from UKNOWN_SOURCE timestamp. Therefore use CLOCK_REALTIME for camera frames timestamp in case
        // we want to sync camera frame timestamp with some other data also in CLOCK_REALTIME.
        int64_t imageTimestampNsecs;
        static struct timespec temp;
        clock_gettime(CLOCK_REALTIME, &temp);
        imageTimestampNsecs = (temp.tv_sec*1e9) + temp.tv_nsec;

        memset(pCaptureResultData, 0, sizeof(CaptureResultFrameData));

        pCaptureResultData->frameNumber    = pHalResult->frame_number;
        pCaptureResultData->timestampNsecs = imageTimestampNsecs;

        // Go through all the output buffers received. It could be preview only, video only, or preview + video
        for (uint32_t i = 0; i < pHalResult->num_output_buffers; i++)
        {
            buffer_handle_t* pImageBuffer;

            if (pHalResult->output_buffers[i].stream == &m_streams[StreamTypePreview])
            {
                pImageBuffer = pHalResult->output_buffers[i].buffer;
                pCaptureResultData->pPreviewBufferInfo = m_pBufferManager[StreamTypePreview]->GetBufferInfo(pImageBuffer);
                m_pBufferManager[StreamTypePreview]->PutBuffer(pImageBuffer); // This queues up the buffer for recycling
            }
            else if (pHalResult->output_buffers[i].stream == &m_streams[StreamTypeVideo])
            {
                pImageBuffer = pHalResult->output_buffers[i].buffer;
                pCaptureResultData->pVideoBufferInfo = m_pBufferManager[StreamTypeVideo]->GetBufferInfo(pImageBuffer);
                m_pBufferManager[StreamTypeVideo]->PutBuffer(pImageBuffer); // This queues up the buffer for recycling

                if (m_nextResultFrameNumber != pHalResult->frame_number)
                {
                    printf("\nHELLOHAL3VIDEO-ERROR: Frame dropped! : %d Received %d",
                            m_nextResultFrameNumber, pHalResult->frame_number);
                    fflush(stdout);
                    m_nextResultFrameNumber = pHalResult->frame_number;
                }
                else
                {
                    m_nextResultFrameNumber++;
                }
            }
        }

        // Mutex is required for msgQueue access from here and from within the thread wherein it will be de-queued
        pthread_mutex_lock(&m_resultThread.mutex);
        // Queue up work for the result thread "ThreadPostProcessResult"
        m_resultThread.msgQueue.push_back((void*)pCaptureResultData);
        pthread_cond_signal(&m_resultThread.cond);
        pthread_mutex_unlock(&m_resultThread.mutex);
    }
}
// -----------------------------------------------------------------------------------------------------------------------------
// Process the result from the camera module. Essentially handle the metadata and the image buffers that are sent back to us.
// We call the PerCameraMgr class function to handle it so that it can have access to any (non-static)class member data it needs
// Remember this function is operating in the camera module thread context. So we should do the bare minimum work and return.
// -----------------------------------------------------------------------------------------------------------------------------
void PerCameraMgr::CameraModuleCaptureResult(const camera3_callback_ops *cb, const camera3_capture_result* pHalResult)
{
    Camera3Callbacks* pCamera3Callbacks = (Camera3Callbacks*)cb;
    PerCameraMgr* pPerCameraMgr = (PerCameraMgr*)pCamera3Callbacks->pPrivate;

    pPerCameraMgr->ProcessOneCaptureResult(pHalResult);
}

// -----------------------------------------------------------------------------------------------------------------------------
// Handle any messages sent to us by the camera module
// -----------------------------------------------------------------------------------------------------------------------------
void PerCameraMgr::CameraModuleNotify(const struct camera3_callback_ops *cb, const camera3_notify_msg_t *msg)
{
    if (msg->type == CAMERA3_MSG_ERROR)
    {
        printf("\nHELLOCAMERA-ERROR: Framenumber: %d ErrorCode: %d",
               msg->message.error.frame_number, msg->message.error.error_code);
    }
}

// -----------------------------------------------------------------------------------------------------------------------------
// Send one capture request to the camera module
// -----------------------------------------------------------------------------------------------------------------------------
int PerCameraMgr::ProcessOneCaptureRequest(int frameNumber)
{
    int status = 0;
    camera3_capture_request_t request = { 0 };
    camera3_stream_buffer_t   streamBuffers[StreamTypeMax];

    if (m_cameraMode == CameraModePreview)
    {
        streamBuffers[0].buffer        = (const native_handle_t**)(m_pBufferManager[StreamTypePreview]->GetBuffer());
        streamBuffers[0].stream        = &m_streams[StreamTypePreview];
        streamBuffers[0].status        = 0;
        streamBuffers[0].acquire_fence = -1;
        streamBuffers[0].release_fence = -1;

        request.num_output_buffers = 1;
        request.output_buffers     = &streamBuffers[0];
    }
    else if (m_cameraMode == CameraModeVideo)
    {
        streamBuffers[0].buffer        = (const native_handle_t**)(m_pBufferManager[StreamTypePreview]->GetBuffer());
        streamBuffers[0].stream        = &m_streams[StreamTypePreview];
        streamBuffers[0].status        = 0;
        streamBuffers[0].acquire_fence = -1;
        streamBuffers[0].release_fence = -1;

        streamBuffers[1].buffer        = (const native_handle_t**)(m_pBufferManager[StreamTypeVideo]->GetBuffer());
        streamBuffers[1].stream        = &m_streams[StreamTypeVideo];
        streamBuffers[1].status        = 0;
        streamBuffers[1].acquire_fence = -1;
        streamBuffers[1].release_fence = -1;

        request.num_output_buffers = 2;
        request.output_buffers     = &streamBuffers[0];
    }

    request.frame_number = frameNumber;
    request.settings     = m_requestMetadata.getAndLock();
    request.input_buffer = NULL;

    // Call the camera module to send the capture request
    status = m_pDevice->ops->process_capture_request(m_pDevice, &request);

    if (status != 0)
    {
        printf("\nHELLOCAMERA-ERROR: Error sending request %d", frameNumber);
    }

    m_requestMetadata.unlock(request.settings);

    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// This function uses OpenCV DNN
// -----------------------------------------------------------------------------------------------------------------------------
void* ThreadARMCL(void* pData)
{
    return NULL;
}

// -----------------------------------------------------------------------------------------------------------------------------
// This function uses OpenCV DNN
// -----------------------------------------------------------------------------------------------------------------------------
void* ThreadDNN(void* pData)
{
    return NULL;
}

// -----------------------------------------------------------------------------------------------------------------------------
// This function uses OpenCV for face detection
// -----------------------------------------------------------------------------------------------------------------------------
void* ThreadFaceDetectAndDraw(void* pData)
{
    return NULL;
}

// -----------------------------------------------------------------------------------------------------------------------------
// PerCameraMgr::CameraModuleCaptureResult(..) is the entry callback that is registered with the camera module to be called when
// the camera module has frame result available to be processed by this application. We do not want to do much processing in
// that function since it is being called in the context of the camera module. So we do the bare minimum processing and leave
// the remaining process upto this function. PerCameraMgr::CameraModuleCaptureResult(..) just pushes a message in a queue that
// is monitored by this thread function. This function goes through the message queue and processes all the messages in it. The
// messages are nothing but camera images that this application has received.
// -----------------------------------------------------------------------------------------------------------------------------
void* ThreadPostProcessResult(void* pData)
{
    // Set thread priority
    pid_t tid = syscall(SYS_gettid);
    int which = PRIO_PROCESS;
    int nice  = -10;
    
    setpriority(which, tid, nice);

    ThreadData*   pThreadData        = (ThreadData*)pData;
    PerCameraMgr* pCameraMgr         = pThreadData->pCameraMgr;
    ThreadData*   pTfLiteThread      = pCameraMgr->TensorflowLiteThreadData();
    int           dumpPreviewFrames  = pCameraMgr->GetDumpPreviewFrames();
    int           previewframeNumber = 0;
    int           videoframeNumber   = -1;
    uint8_t*      pRaw8bit           = NULL;

    if (pCameraMgr->IsVideoMode())
    {
        videoframeNumber = 0;
    }

    // The condition of the while loop is such that this thread will not terminate till it receives the last expected image
    // frame from the camera module
    while ((pThreadData->lastResultFrameNumber != previewframeNumber) ||
           ((videoframeNumber != -1) && (pThreadData->lastResultFrameNumber != videoframeNumber)))
    {
        pthread_mutex_lock(&pThreadData->mutex);

        if (pThreadData->msgQueue.empty()) 
        {
            struct timespec tv;
            clock_gettime(CLOCK_MONOTONIC, &tv);
            tv.tv_sec += 1;

            // Go to a temporary small sleep waiting for the result frame to arrive
            if(pthread_cond_timedwait(&pThreadData->cond, &pThreadData->mutex, &tv) != 0) 
            {
                pthread_mutex_unlock(&pThreadData->mutex);
                continue;
            }
        }

        // Coming here means we have a result frame to process

        CaptureResultFrameData* pCaptureResultData = (CaptureResultFrameData*)pThreadData->msgQueue.front();

        pThreadData->msgQueue.pop_front();
        pthread_mutex_unlock(&pThreadData->mutex);

        // Handle dumping preview frames to files
        if (pCaptureResultData->pPreviewBufferInfo != NULL)
        {
            // Currently all frames are sent for object detection, but we could easily change that to any number like every 5th frame
            // or something like that
            if (pCaptureResultData->frameNumber % 1 == 0)
            {
                TensorflowMessage* pTensorflowMessage = new TensorflowMessage;
                memset(pTensorflowMessage, 0, sizeof(TensorflowMessage));
                pTensorflowMessage->frameNumber = pCaptureResultData->frameNumber;
                pTensorflowMessage->pBufferInfo = pCaptureResultData->pPreviewBufferInfo;
        
                // Mutex is required for msgQueue access from here and from within the thread wherein it will be de-queued
                pthread_mutex_lock(&pTfLiteThread->mutex);
                // Queue up work for the result thread "TensorflowThread"
                pTfLiteThread->msgQueue.push_back((void*)pTensorflowMessage);
                pthread_cond_signal(&pTfLiteThread->cond);
                pthread_mutex_unlock(&pTfLiteThread->mutex);
            }

            if (dumpPreviewFrames > 0)
            {
                BufferInfo* pBufferInfo     = pCaptureResultData->pPreviewBufferInfo;
                uint8_t*    pImagePixels    = (uint8_t*)pBufferInfo->vaddr;
                static int  previewFrameNum = 0;

                dumpPreviewFrames--;
                char filename[256] = { '\0' };

                if (pBufferInfo->format == HAL_PIXEL_FORMAT_RAW10)
                {
                    sprintf(&filename[0], "%s_%d.yuv", "image_preview_raw", previewFrameNum++);

                    uint32_t imageSizePixels = pBufferInfo->width * pBufferInfo->height;

                    if (pRaw8bit == NULL)
                    {
                        pRaw8bit = (uint8_t*)malloc(imageSizePixels  * 1.25); // 1.25 because its 10 bits/pixel
                    }

                    uint8_t* pSrcPixel  = (uint8_t*)pBufferInfo->vaddr;
                    uint8_t* pDestPixel = pRaw8bit;
                    pImagePixels        = pRaw8bit;

                    // This link has the description of the RAW10 format:
                    // https://gitlab.com/SaberMod/pa-android-frameworks-base/commit/d1988a98ed69db8c33b77b5c085ab91d22ef3bbc
                    for (unsigned int i = 0; i < imageSizePixels; i+=4)
                    {
                        uint8_t* pFifthByte = pSrcPixel + 4;
                        uint8_t  pixel0     = ((*pFifthByte & 0xC0) >> 6);
                        uint8_t  pixel1     = ((*pFifthByte & 0x30) >> 4);
                        uint8_t  pixel2     = ((*pFifthByte & 0x0C) >> 2);
                        uint8_t  pixel3     = ((*pFifthByte & 0x03) >> 0);
                        uint32_t temp;
                        uint32_t Max = 255;
                        uint32_t tempPixel;

                        tempPixel = *pSrcPixel;
                        tempPixel <<= 2;
                        temp = tempPixel+pixel0;
                        *pDestPixel = (uint8_t)std::min(Max, temp);
                        pDestPixel++;
                        pSrcPixel++;

                        tempPixel = *pSrcPixel;
                        tempPixel <<= 2;
                        temp = tempPixel+pixel1;
                        *pDestPixel = (uint8_t)std::min(Max, temp);
                        pDestPixel++;
                        pSrcPixel++;

                        tempPixel = *pSrcPixel;
                        tempPixel <<= 2;
                        temp = tempPixel+pixel2;
                        *pDestPixel = (uint8_t)std::min(Max, temp);
                        pDestPixel++;
                        pSrcPixel++;

                        tempPixel = *pSrcPixel;
                        tempPixel <<= 2;
                        temp = tempPixel+pixel3;
                        *pDestPixel = (uint8_t)std::min(Max, temp);
                        pDestPixel++;
                        pSrcPixel++;

                        pSrcPixel++;
                    }

                    FILE* fd = fopen(&filename[0], "wb");
                    fwrite(pImagePixels, pCaptureResultData->pPreviewBufferInfo->size, 1, fd);
                    fclose(fd);
                }
                else if (pBufferInfo->format == HAL_PIXEL_FORMAT_BLOB)
                {
                    sprintf(&filename[0], "%s_%d.yuv", "image_preview_blob", previewFrameNum++);

                    FILE* fd = fopen(&filename[0], "wb");
                    fclose(fd);
                    fd = fopen(&filename[0], "a");

                    pImagePixels = (uint8_t*)pBufferInfo->vaddr;
                    fwrite(pImagePixels, pCaptureResultData->pPreviewBufferInfo->size, 1, fd);
                    fclose(fd);
                }
                else
                {
                    sprintf(&filename[0], "%s_%d.yuv", "image_preview_nv21", previewFrameNum++);

                    FILE* fd = fopen(&filename[0], "wb");
                    fclose(fd);
                    fd = fopen(&filename[0], "a");

                    pImagePixels = (uint8_t*)pBufferInfo->vaddr;

                    if (pBufferInfo->stride == (int)pBufferInfo->width)
                    {
                        fwrite(pImagePixels, (pBufferInfo->width * pBufferInfo->height), 1, fd);
                        pImagePixels = (uint8_t*)pBufferInfo->craddr;
                        // CbCr are subsampled so that height of the UV plane is UV/2
                        fwrite(pImagePixels, (pBufferInfo->width * (pBufferInfo->height/2)), 1, fd);
                    }
                    else
                    {
                        for (uint32_t i = 0; i < pBufferInfo->height; i++)
                        {
                            fwrite(pImagePixels, pBufferInfo->width, 1, fd);
                            pImagePixels += pBufferInfo->stride;
                        }

                        pImagePixels = (uint8_t*)pBufferInfo->craddr;
                        uint32_t uvHeight = pBufferInfo->height/2;

                        for (uint32_t i = 0; i < uvHeight; i++)
                        {
                            fwrite(pImagePixels, pBufferInfo->width, 1, fd);
                            pImagePixels += pBufferInfo->stride;
                        }
                    }
                    fclose(fd);
                }
            }

            previewframeNumber = pCaptureResultData->frameNumber;
        }

        delete pCaptureResultData;
    }

    if (pRaw8bit != NULL)
    {
        free(pRaw8bit);
    }

    printf("\n========= Last result frame: %d", previewframeNumber);
    fflush(stdout);

    return NULL;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Main thread function to initiate the sending of capture requests to the camera module. Keeps on sending the capture requests
// to the camera module till a "stop message" is passed to this thread function
// -----------------------------------------------------------------------------------------------------------------------------
void* ThreadIssueCaptureRequests(void* data)
{
    ThreadData*   pThreadData = (ThreadData*)data;
    PerCameraMgr* pCameraMgr  = pThreadData->pCameraMgr;
    // Set thread priority
    pid_t tid = syscall(SYS_gettid);
    int which = PRIO_PROCESS;
    int nice  = -10;

    int frame_number = -1;
    
    setpriority(which, tid, nice);

    while (!pThreadData->stop)
    {
        pCameraMgr->ProcessOneCaptureRequest(++frame_number);
    }

    // Stop message received. Inform about the last framenumber requested from the camera module. This in turn will be used
    // by the result thread to wait for this frame's image buffers to arrive.
    pCameraMgr->StoppedSendingRequest(frame_number);
    printf("\n========= Last request frame: %d", frame_number);
    return NULL;
}

// -----------------------------------------------------------------------------------------------------------------------------
// This function is called to indicate that the request sending thread has issued the last request and no more capture requests
// will be sent
// -----------------------------------------------------------------------------------------------------------------------------
void PerCameraMgr::StoppedSendingRequest(int framenumber)
{
    m_resultThread.lastResultFrameNumber = framenumber;
}
