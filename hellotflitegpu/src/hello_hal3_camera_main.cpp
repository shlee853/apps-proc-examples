/*******************************************************************************************************************************
 *
 * Copyright (c) 2019 ModalAI, Inc.
 *
 ******************************************************************************************************************************/
#include <errno.h>
#include <getopt.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hello_hal3_camera.h"

// Function prototypes
void PrintHelpMessage();
int  ErrorCheck(int numInputsScanned, const char* pOptionName);
int ParseArgs(int         argc,
              char* const pArgv[],
              int*        pCameraid,
              int*        pWidth,
              int*        pHeight,
              int*        pDumpPreviewFrames,
              char*       pIPAddress,
              char*       pDnnModelFile,
              char*       pLabelsFile);

void CtrlCHandler(int dummy);

volatile int g_keepRunning = 1;

// -----------------------------------------------------------------------------------------------------------------------------
// Main
// -----------------------------------------------------------------------------------------------------------------------------
int main(int argc, char* const argv[])
{
    CameraHAL3* pCameraHAL3 = new CameraHAL3;
    int         status;
    // All the default values can be overwritten with the command line args. If the command line args is not specified, the
    // defaults are used
    int         cameraid           = 0;
    int         width              = 640;
    int         height             = 480;
    int         format             = PreviewFormatNV21;
    int         mode               = CameraModePreview;
    int         dumpPreviewFrames  = 0;
    char        ipAddress[256]     = "192.168.1.159";
    char        dnnModelFile[256]  = "/bin/dnn/mobilenet_v1_ssd_coco_labels.tflite";
    char        dnnLabelsFile[256] = "/bin/dnn/mobilenet_v1_ssd_coco_labels.txt";

    signal(SIGINT, CtrlCHandler);

    status = ParseArgs(argc,
                       argv,
                       &cameraid,
                       &width,
                       &height,
                       &dumpPreviewFrames,
                       &ipAddress[0],
                       &dnnModelFile[0],
                       &dnnLabelsFile[0]);

    if (status == 0)
    {
        printf("\nCamera id: %d", cameraid);
        printf("\nImage width: %d", width);
        printf("\nImage height: %d", height);
        printf("\nNumber of frames to dump: %d", dumpPreviewFrames);
        printf("\nIP Address to stream to: %s", &ipAddress[0]);
        printf("\nDNN Model: %s", &dnnModelFile[0]);
        printf("\nDNN Labels: %s", &dnnLabelsFile[0]);

        if (mode == CameraModePreview)
        {
            printf("\nCamera mode: preview");
        }
        else if (mode == CameraModeVideo)
        {
            printf("\nCamera mode: preview + video\n");
        }

        status = pCameraHAL3->Initialize();
    }
    else
    {
        PrintHelpMessage();
    }

    if (status == 0)
    {
        // Making this function call will start the camera (cameraid) and it will start streaming frames
        status = pCameraHAL3->Start(cameraid,
                                    width,
                                    height,
                                    (PreviewFormat)format,
                                    (CameraMode)mode,
                                    dumpPreviewFrames,
                                    &ipAddress[0],
                                    &dnnModelFile[0],
                                    &dnnLabelsFile[0]);
    }

    if (status == 0)
    {
        printf("\n\nCamera HAL3 test is now running");
        fflush(stdout);

        // The camera keeps producing frames till the user presses Ctrl+C to terminate the program
        while (g_keepRunning)
        {
            usleep(1e5);
        }

        printf("\nCamera HAL3 test is now stopping");
        pCameraHAL3->Stop();
        printf("\nCamera HAL3 test is done");
    }

    delete pCameraHAL3;

    printf("\n\n");
    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Parses the command line arguments to the main function
// -----------------------------------------------------------------------------------------------------------------------------
int ParseArgs(int         argc,
              char* const pArgv[],
              int*        pCameraid,
              int*        pWidth,
              int*        pHeight,
              int*        pDumpPreviewFrames,
              char*       pIPAddress,
              char*       pDnnModelFile,
              char*       pLabelsFile)

{
    static struct option LongOptions[] =
    {
        {"cameraid",     required_argument, 0, 'c'},
        {"width",        required_argument, 0, 'W'},
        {"height",       required_argument, 0, 'H'},
        {"dumppreview",  required_argument, 0, 'd'},
        {"ipaddress",    required_argument, 0, 'i'},
        {"dnnmodel",     required_argument, 0, 'm'},
        {"labels",       required_argument, 0, 'l'},
        {"help",         no_argument,       0, 'h'},
        {0, 0, 0, 0                               }
    };

    int numInputsScanned = 0;
    int optionIndex      = 0;
    int status           = 0;    
    int option;

    while ((status == 0) && (option = getopt_long_only (argc, pArgv, ":c:W:H:d:i:m:l:h", &LongOptions[0], &optionIndex)) != -1)
    {
        switch(option)
        {
            case 'c':
                numInputsScanned = sscanf(optarg, "%d", pCameraid);
                
                if (ErrorCheck(numInputsScanned, LongOptions[optionIndex].name) != 0)
                {
                    printf("\nNo cameraId specified!");
                    status = -EINVAL;
                }
               
                break;

            case 'W':
                numInputsScanned = sscanf(optarg, "%d", pWidth);
                
                if (ErrorCheck(numInputsScanned, LongOptions[optionIndex].name) != 0)
                {
                    printf("\nNo width specified");
                    status = -EINVAL;
                }
                
                break;

            case 'H':
                numInputsScanned = sscanf(optarg, "%d", pHeight);
                
                if (ErrorCheck(numInputsScanned, LongOptions[optionIndex].name) != 0)
                {
                    printf("\nNo height specified");
                    status = -EINVAL;
                }
                
                break;

            case 'd':
                numInputsScanned = sscanf(optarg, "%d", pDumpPreviewFrames);
                
                if (ErrorCheck(numInputsScanned, LongOptions[optionIndex].name) != 0)
                {
                    printf("\nNo preview dump frames specified");
                    status = -EINVAL;
                }
                
                break;

            case 'i':
                numInputsScanned = sscanf(optarg, "%s", pIPAddress);
                
                if (ErrorCheck(numInputsScanned, LongOptions[optionIndex].name) != 0)
                {
                    printf("\nNo IP address specified");
                    status = -EINVAL;
                }
                
                break;

            case 'm':
                numInputsScanned = sscanf(optarg, "%s", pDnnModelFile);
                
                if (ErrorCheck(numInputsScanned, LongOptions[optionIndex].name) != 0)
                {
                    printf("\nNo DNN model filename specified");
                    status = -EINVAL;
                }
                
                break;

            case 'l':
                numInputsScanned = sscanf(optarg, "%s", pLabelsFile);
                
                if (ErrorCheck(numInputsScanned, LongOptions[optionIndex].name) != 0)
                {
                    printf("\nNo DNN model labels filename specified");
                    status = -EINVAL;
                }
                
                break;

            case 'h':
                status = -EINVAL; // This will have the effect of printing the help message and exiting the program
                break;

            // Unknown argument
            case '?':
            default:
                printf("\nInvalid argument passed!");
                status = -EINVAL;
                break;
        }
    }

    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Print the help message
// -----------------------------------------------------------------------------------------------------------------------------
void PrintHelpMessage()
{
    printf("\n\nCommand line arguments are as follows:\n");
    printf("\n-c : camera id (Default 0)");
    printf("\n-W : frame width (Default 640)");
    printf("\n-H : frame height (Default 480)");
    printf("\n-i : IP address of the VOXL to stream the object detected RGB image (Default: 192.168.1.159)");
    printf("\n\t : -i 0 to disable streaming");
    printf("\n-m : Filename of the deep learning model to use (Default: /bin/dnn/mobilenet_v1_ssd_coco_labels.tflite)");
    printf("\n-l : Filename of the class labels for the model (Default: /bin/dnn/mobilenet_v1_ssd_coco_labels.txt)");
    printf("\n-d : Dump 'n' preview frames (Default is 0)");
    printf("\n-h : Print this help message");
    printf("\n\nFor example: hello-voxl-tflite-gpu -c 0 -W 1920 -H 1080 -i 192.168.1.159");
}

// -----------------------------------------------------------------------------------------------------------------------------
// Check for error in parsing the arguments
// -----------------------------------------------------------------------------------------------------------------------------
int ErrorCheck(int numInputsScanned, const char* pOptionName)
{
    int error = 0;

    if (numInputsScanned != 1)
    {
        fprintf(stderr, "ERROR: Invalid argument for %s option\n", pOptionName);
        error = -1;
    }

    return error;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Ctrl+C handler that will stop the camera and exit the program gracefully
// -----------------------------------------------------------------------------------------------------------------------------
void CtrlCHandler(int dummy)
{
    g_keepRunning = 0;
    signal(SIGINT, NULL);
}
