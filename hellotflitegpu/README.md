# HelloTfliteGPU sample app to run Tensor Flow Lite Deep Learning models on live camera frames using OpenCL for Linux GPU acceleration

## Dependencies:
-----------------------
1. Requires the voxl-emulator (found [here](https://gitlab.com/voxl-public/voxl-docker)) to run docker ARM image
1. Download hello-tflite-gpu-libs_0.0.1.ipk required to build the app (found [here](https://developer.modalai.com/asset/eula-download/48))    
1. Uses OpenCV 2.8.1 found on the emulator and on target. Minor code changes needed to move to a new version of OpenCV

## Build sample project:
-----------------------
1. ```$ cp [Path To]/hello-tflite-gpu-libs_0.0.1.ipk [Path To]/apps-proc-examples/hellotflitegpu/```
1. ```$ voxl-docker -d [Path To]/apps-proc-examples/hellotflitegpu```
1. ```$ opkg install hello-tflite-gpu-libs_0.0.1.ipk```
    * The install will create a directory called "modalai-hellotflite-gpu"
1. RUN make
    * This will generate a binary called "hello-voxl-tflite-gpu"
    * You can use "make clean" to clean any previous build
1. RUN ./install_on_voxl.sh to install the required files on VOXL
1. adb shell to go on VOXL
1. RUN hello-voxl-tflite-gpu -c "HIRES-CAMERA_ID" -W 1024 -H 768 (e.g. hello-voxl-tflite-gpu -c 0 -W 1024 -H 768)
    * For help with command line RUN hello-voxl-tflite-gpu -h
1. Open another terminal window
1. adb shell
1. RUN "voxl-perfmon" to check GPU usage
1. Open another terminal window
1. ```cd [Path To]/apps-proc-examples/hellotflitegpu/```
1. RUN "python image_viewer_rgb.py --i IP-ADDRESS-OF-VOXL"
    * Requires WiFi setup, instructions found [here](https://docs.modalai.com/wifi-setup/)
    * Requires python-qt4 package on PC: "sudo apt-get install python-qt4"
    * For IP address, adb shell into VOXL and RUN ifconfig
