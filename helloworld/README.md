# HelloWorld Sample

## Build sample project:
---------------------
1. Requires the voxl-emulator (found [here](https://gitlab.com/voxl-public/voxl-docker)) to run docker ARM image
    * cd [Path To]/voxl-docker
    * voxl-docker -d [Path To]/apps-proc-examples/helloworld
2. Create build directory:
    * mkdir build
    * cd build
3. Build project binary:
    * cmake ../
    * make
4. Push binary to target:
    * adb push helloworld /home/root
