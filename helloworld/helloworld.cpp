/****************************************************************************
 *
 * Copyright (c) 2019 ModalAI, Inc.
 *
 ****************************************************************************/

#include <iostream>

using namespace std;

int main() 
{
    cout << "\"Hello, World!\" from App Processor" << endl;
    return 0;
}