# HelloVideo sample using the HAL3 interface

## Build sample project:
-----------------------
1. Requires the voxl-emulator (found [here](https://gitlab.com/voxl-public/voxl-docker)) to run docker ARM image
    * cd [Path To]/voxl-docker
    * voxl-docker -d [Path To]/apps-proc-examples/hellovideo
2. Build project binary:
    * make
3. Push binary to target:
    * adb push hello_hal3_video /bin
4. Run on target:
    * adb shell
    * hello_hal3_video -h
    * hello_hal3_video -c 0 -W 1920 -H 1080 -m 1 -v ./hal3_video.h265.mp4
    * Press Ctrl+C to stop the program anytime
5. Video can be viewed with video players like SMPlayer 
6. For facedetect setup your target with OpenCV (Follow "Build" section found [here](https://gitlab.com/voxl-public/voxl-opencv-3-4-6))
    * To enable facedetect feature, run the application with the "-fd 1" command line option
    * Use the "-h" command line option for more info on facedetect
7. For running the frame through DNN setup your target with OpenCV (Follow "Build" section found [here](https://gitlab.com/voxl-public/voxl-opencv-3-4-6)
    * The DNN models are in the ./dnn folder
    * adb push MobileNetSSD_deploy.caffemodel /bin/dnn
    * adb push MobileNetSSD_deploy.prototxt.txt /bin/dnn
    * To enable DNN, run the application with the "-dnn 1" command line option
    * Use the "-h" command line option for more info on DNN

