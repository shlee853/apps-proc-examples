/*******************************************************************************************************************************
 *
 * Copyright (c) 2019 ModalAI, Inc.
 *
 ******************************************************************************************************************************/

#ifndef HELLO_HAL3_CAMERA_VIDEO_ENCODER
#define HELLO_HAL3_CAMERA_VIDEO_ENCODER

#include <list>
#include <OMX_Core.h>
#include <OMX_IVCommon.h>
#include <pthread.h>
#include <stdint.h>
#include <stdio.h>

// Forward Declaration
class  VideoEncoder;
struct BufferInfo;
// -----------------------------------------------------------------------------------------------------------------------------
// Thread Data for OMX threads
// -----------------------------------------------------------------------------------------------------------------------------
typedef struct OMXThreadData
{
    pthread_t         thread;               ///< Thread handle
    pthread_mutex_t   mutex;                ///< Mutex for list access
    pthread_cond_t    cond;                 ///< Condition variable for wake up
    std::list<void*>  msgQueue;             ///< Message queue
    VideoEncoder*     pVideoEncoder;        ///< Pointer to the VideoEncoder object
    volatile int      lastFrameNumber;      ///< The absolute last frame that needs to be encoded
    volatile bool     stop;                 ///< Thread terminate indicator
} OMXThreadData;

// -----------------------------------------------------------------------------------------------------------------------------
// Video encoder config data
// -----------------------------------------------------------------------------------------------------------------------------
typedef struct VideoEncoderConfig
{
    uint32_t width;                 ///< Image width
    uint32_t height;                ///< Image height
    uint32_t format;                ///< Image format
    bool     isBitRateConstant;     ///< Is the bit rate constant
    int      targetBitRate;         ///< Desired target bitrate
    int32_t  frameRate;             ///< Frame rate
    bool     isH265;                ///< Is it H265 encoding or H264
    FILE*    pVideoFileHandle;      ///< Video file handle to write to
} VideoEncoderConfig;

//------------------------------------------------------------------------------------------------------------------------------
// Main interface class that interacts with the OMX Encoder component and the Camera Manager class. At the crux of it, this
// class takes the YUV frames from the camera and passes it to the OMX component for encoding. It writes the encoded frames
// to the final (H264/5) video file.
//------------------------------------------------------------------------------------------------------------------------------
class VideoEncoder
{
public:
    VideoEncoder();
    ~VideoEncoder();

    // Get the timestamp increments
    int64_t GetTimestampIncrements() const
    {
        return m_frameTimestampInc;
    }
    // Get OMX Handle
    OMX_HANDLETYPE GetOMXHandle()
    {
        return m_OMXHandle;
    }
    // Get the OMX Input thread data
    OMXThreadData* GetOMXOutputThreadData()
    {
        return &m_OMXOutPortThread;
    }
    // Perform any one time initialization
    int  Initialize(VideoEncoderConfig* pVideoEncoderConfig);
    // Do any necessary work to start receiving encoding frames from the client
    void Start();
    // This call indicates that no more frames will be sent for encoding
    void Stop();
    // Client of this encoder class calls this function to pass in the YUV video frame to be encoded
    void ProcessFrameToEncode(int frameNumber, BufferInfo* pBufferToEncode);
    // OMX output thread calls this function to process the OMX component's output encoded buffer
    void ProcessEncodedFrame(OMX_BUFFERHEADERTYPE* pEncodedFrame);

private:
    // Set the OMX component configuration
    OMX_ERRORTYPE SetConfig(VideoEncoderConfig* pVideoEncoderConfig);
    // Set input / output port parameters
    OMX_ERRORTYPE SetPortParams(OMX_U32  portIndex,
                                OMX_U32  width,
                                OMX_U32  height,
                                OMX_U32  bufferCountMin,
                                OMX_U32  frameRate,
                                OMX_U32* pBufferSize,
                                OMX_U32* pBufferCount,
                                OMX_COLOR_FORMATTYPE format);

    static const uint32_t NumOMXInputBuffers   = 16;
    static const uint32_t NumOMXOutputBuffers  = 16;
    static const uint32_t InputBufferCount     = 20;
    static const uint32_t OutputBufferCount    = 20;
    static const uint32_t BitrateDefault       = (2*8*1024*1024);
    static const uint32_t TargetBitrateDefault = (18*1024*1024*8);
    static const OMX_U32  PortIndexIn          = 0;
    static const OMX_U32  PortIndexOut         = 1;

    uint32_t               m_inputBufferSize;       ///< Input buffer size
    uint32_t               m_inputBufferCount;      ///< Input buffer count
    uint32_t               m_outputBufferSize;      ///< Output buffer size
    uint32_t               m_outputBufferCount;     ///< Output buffer count
    const char*            m_pVideoFilename;        ///< Video filename
    FILE*                  m_pVideoFilehandle;      ///< Video file handle
    OMXThreadData          m_OMXInPortThread;       ///< OMX input port thread private data
    OMXThreadData          m_OMXOutPortThread;      ///< OMX output port thread private data
    OMX_HANDLETYPE         m_OMXHandle;             ///< OMX component handle
    OMX_BUFFERHEADERTYPE** m_ppInputBuffers;        ///< Input buffers
    uint32_t               m_nextInputBufferIndex;  ///< Next input buffer to use
    OMX_BUFFERHEADERTYPE** m_ppOutputBuffers;       ///< Output buffers
    uint32_t               m_nextOutputBufferIndex; ///< Next input buffer to use
    int64_t                m_frameTimestampInc;     ///< Frame timestamp increments based on framerate
};

#endif // HELLO_HAL3_CAMERA_VIDEO_ENCODER
