/*******************************************************************************************************************************
 *
 * Copyright (c) 2019 ModalAI, Inc.
 *
 ******************************************************************************************************************************/
#include <OMX_Component.h>
#include <OMX_IndexExt.h>
#include <OMX_VideoExt.h>
#include <cstring>
#include <unistd.h>
#include <sys/resource.h>
#include <sys/syscall.h>
#include <errno.h>
#include <system/graphics.h>
#include <HardwareAPI.h>
#include "OMX_QCOMExtns.h"
#include "hello_hal3_video_encoder.h"
#include "buffer_manager.h"

#define Log2(number, power) {OMX_U32 temp = number; power = 0; while ((0 == (temp & 0x1)) && power < 16) {temp >>=0x1; power++;}}
#define FractionToQ16(q,num,den) { OMX_U32 power; Log2(den,power); q = num << (16 - power); }

static int64_t g_totalNsecsEncodedFrames = 0;

static const int32_t OMXSpecVersion = 0x00000101;

// Helper MACRO to reset the size, version of any OMX structure
#define OMX_RESET_STRUCT_SIZE_VERSION(_structPointer_, _structName_)    \
    (_structPointer_)->nSize = sizeof(_structName_);                    \
    (_structPointer_)->nVersion.nVersion = OMXSpecVersion

// Helper MACRO to reset any OMX structure to its default valid state
#define OMX_RESET_STRUCT(_structPointer_, _structName_)     \
    memset((_structPointer_), 0x0, sizeof(_structName_));   \
    (_structPointer_)->nSize = sizeof(_structName_);        \
    (_structPointer_)->nVersion.nVersion = OMXSpecVersion

// Main thread functions for providing input buffer to the OMX input port and processing encoded buffer on the OMX output port
void* ThreadProcessOMXInputPort(void* data);
void* ThreadProcessOMXOutputPort(void* data);

// Function called by the OMX component for event handling
OMX_ERRORTYPE OMXEventHandler(OMX_IN OMX_HANDLETYPE hComponent,
                              OMX_IN OMX_PTR        pAppData,
                              OMX_IN OMX_EVENTTYPE  eEvent,
                              OMX_IN OMX_U32        nData1,
                              OMX_IN OMX_U32        nData2,
                              OMX_IN OMX_PTR        pEventData);

// Function called by the OMX component to indicate the input buffer we passed to it has been consumed
OMX_ERRORTYPE OMXEmptyBufferHandler(OMX_IN OMX_HANDLETYPE        hComponent,
                                    OMX_IN OMX_PTR               pAppData,
                                    OMX_IN OMX_BUFFERHEADERTYPE* pBuffer);

// Function called by the OMX component to hand over the encoded output buffer
OMX_ERRORTYPE OMXFillHandler(OMX_OUT OMX_HANDLETYPE        hComponent,
                             OMX_OUT OMX_PTR               pAppData,
                             OMX_OUT OMX_BUFFERHEADERTYPE* pBuffer);

// -----------------------------------------------------------------------------------------------------------------------------
// This is the message structure for the OMX input port thread handler
// -----------------------------------------------------------------------------------------------------------------------------
struct OMXInPortThreadMessageData
{
    BufferInfo*           pInputFrame;  /// Input frame to encode
    OMX_BUFFERHEADERTYPE* pOMXBuffer;   ///< OMX buffer to give on the input port of the OMX component
    int                   frameNumber;  ///< Framenumber being encoded
};

// -----------------------------------------------------------------------------------------------------------------------------
// This is the message structure for the OMX output port thread handler
// -----------------------------------------------------------------------------------------------------------------------------
struct OMXOutPortThreadMessageData
{
    OMX_BUFFERHEADERTYPE* pOMXBuffer;   ///< Encoded OMX buffer given to us by the OMX component
};

// -----------------------------------------------------------------------------------------------------------------------------
// Constructor
// -----------------------------------------------------------------------------------------------------------------------------
VideoEncoder::VideoEncoder()
{
    m_pVideoFilename   = NULL;
    m_pVideoFilehandle = NULL;

    m_OMXInPortThread.pVideoEncoder = NULL;
    m_OMXInPortThread.msgQueue.clear();
    m_OMXInPortThread.stop = false;

    m_OMXOutPortThread.lastFrameNumber = -1;
    m_OMXOutPortThread.pVideoEncoder   = NULL;
    m_OMXOutPortThread.msgQueue.clear();
    m_OMXOutPortThread.stop = false;

    m_OMXHandle = NULL;

    m_inputBufferSize   = 0;
    m_inputBufferCount  = 0;
    m_outputBufferSize  = 0;
    m_outputBufferCount = 0;

    m_nextInputBufferIndex  = 0;
    m_nextOutputBufferIndex = 0;

    m_frameTimestampInc = 0;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Destructor
// -----------------------------------------------------------------------------------------------------------------------------
VideoEncoder::~VideoEncoder()
{
    for (uint32_t i = 0; i < m_inputBufferCount; i++)
    {
        OMX_FreeBuffer(m_OMXHandle, PortIndexIn, m_ppInputBuffers[i]);
    }

    delete m_ppInputBuffers;

    for (uint32_t i = 0; i < m_outputBufferCount; i++)
    {
        OMX_FreeBuffer(m_OMXHandle, PortIndexOut, m_ppOutputBuffers[i]);
    }

    delete m_ppOutputBuffers;

    if (m_OMXHandle != NULL)
    {
        OMX_FreeHandle(m_OMXHandle);
        m_OMXHandle = NULL;
    }
}

// -----------------------------------------------------------------------------------------------------------------------------
// Performs any one time initialization. This function should only be called once.
// -----------------------------------------------------------------------------------------------------------------------------
int VideoEncoder::Initialize(VideoEncoderConfig* pVideoEncoderConfig)
{
    int status = 0;
    OMX_ERRORTYPE omxStatus = OMX_ErrorNone;

    omxStatus = OMX_Init();

    if (omxStatus != OMX_ErrorNone)
    {
        printf("HELLOHAL3VIDEO-ERROR: OMX Init failed!");
        status = -EINVAL;
    }

    if (status == 0)
    {
        // This initializes the OMX component completely and makes the OMX component ready for use
        omxStatus = SetConfig(pVideoEncoderConfig);

        if (omxStatus != OMX_ErrorNone)
        {
            status = -EINVAL;
        }
    }

    if (status == 0)
    {
        m_pVideoFilehandle = pVideoEncoderConfig->pVideoFileHandle;

        // This results in the OMX component i.e. the video encoder to begin processing input and output frames
        omxStatus = OMX_SendCommand(m_OMXHandle, OMX_CommandStateSet, (OMX_U32)OMX_StateExecuting, NULL);

        if (omxStatus == OMX_ErrorNone)
        {
            // We do this so that the OMX component has output buffers to fill the encoded frame data. The output buffers are
            // recycled back to the OMX component once they are returned to us and after we write the frame content to the
            // video file
            for (uint32_t i = 0;  i < m_outputBufferCount; i++)
            {
                // Video encoder fills these buffers with the encoded frame data
                omxStatus = OMX_FillThisBuffer(m_OMXHandle, m_ppOutputBuffers[i]);

                if (omxStatus != OMX_ErrorNone)
                {
                    break;
                }
            }
        }
    }

    if (omxStatus == OMX_ErrorNone)
    {
        m_OMXInPortThread.pVideoEncoder  = this;
        m_OMXOutPortThread.pVideoEncoder = this;
    }
    else
    {
        status = -EINVAL;
    }

    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// This configures the OMX component i.e. the video encoder's input, output ports and all its parameters and gets it into a
// ready to use state. After this function we can start sending input buffers to the video encoder and it will start sending
// back the encoded frames
// -----------------------------------------------------------------------------------------------------------------------------
OMX_ERRORTYPE VideoEncoder::SetConfig(VideoEncoderConfig* pVideoEncoderConfig)
{
    OMX_ERRORTYPE                    status    = OMX_ErrorNone;
    OMX_CALLBACKTYPE                 callbacks = {OMXEventHandler, OMXEmptyBufferHandler, OMXFillHandler};
    OMX_COLOR_FORMATTYPE             omxFormat = OMX_COLOR_FormatMax;    
    OMX_VIDEO_PARAM_PORTFORMATTYPE   videoPortFmt;
    OMX_VIDEO_PARAM_PROFILELEVELTYPE profileLevel;
    int  codingType;
    int  profile;
    int  level;
    bool isFormatSupported = false;    

    OMX_RESET_STRUCT(&videoPortFmt, OMX_VIDEO_PARAM_PORTFORMATTYPE);
    OMX_RESET_STRUCT(&profileLevel, OMX_VIDEO_PARAM_PROFILELEVELTYPE);

    char* pComponentName;

    if (pVideoEncoderConfig->isH265 == true)
    {
        pComponentName = (char *)"OMX.qcom.video.encoder.hevc";
        codingType     = OMX_VIDEO_CodingHEVC;
        profile        = OMX_VIDEO_HEVCProfileMain;
        level          = OMX_VIDEO_HEVCHighTierLevel3;
    }
    else
    {
        pComponentName = (char *)"OMX.qcom.video.encoder.avc";
        codingType     = OMX_VIDEO_CodingAVC;
        profile        = OMX_VIDEO_AVCProfileBaseline;
        level          = OMX_VIDEO_AVCLevel3;
    }

    status = OMX_GetHandle(&m_OMXHandle, pComponentName, this, &callbacks);

    if (status == OMX_ErrorNone)
    {
        if (pVideoEncoderConfig->format == HAL_PIXEL_FORMAT_YCbCr_420_888)
        {
            omxFormat = OMX_QCOM_COLOR_FormatYVU420SemiPlanar;
        }
        else
        {
            fprintf(stderr, "\nHELLOHAL3VIDEO-ERROR: Unknown video recording format!");
            status = OMX_ErrorBadParameter;
        }
    }
    else
    {
        printf("\nHELLOHAL3VIDEO-ERROR: OMX_GetHandle failed!");
    }

    // Check if OMX component supports the input frame format
    if (status == OMX_ErrorNone)
    {
        OMX_S32 index = 0;

        while (status == OMX_ErrorNone)
        {
            videoPortFmt.nPortIndex = PortIndexIn;
            videoPortFmt.nIndex     = index;

            status = OMX_GetParameter(m_OMXHandle, OMX_IndexParamVideoPortFormat, (OMX_PTR)&videoPortFmt);

            if ((status == OMX_ErrorNone) && (videoPortFmt.eColorFormat == omxFormat))
            {
                isFormatSupported = true;
                break;
            }

            index++;
        }
    }

    if (isFormatSupported == false)
    {
        status = OMX_ErrorBadParameter;
        printf("\nHELLOHAL3VIDEO-ERROR: Unsupported video input format in OMX component!");
    }
    else
    {
        // Configure either for H265 or H264
        if (codingType == OMX_VIDEO_CodingAVC)
        {
            OMX_VIDEO_PARAM_AVCTYPE avc;

            OMX_RESET_STRUCT(&avc, OMX_VIDEO_PARAM_AVCTYPE);
            avc.nPortIndex = PortIndexOut;

            status = OMX_GetParameter(m_OMXHandle, OMX_IndexParamVideoAvc, (OMX_PTR)&avc);

            if (status != OMX_ErrorNone)
            {
                printf("\nHELLOHAL3VIDEO-ERROR: OMX_GetParameter of OMX_IndexParamVideoAvc failed!");
            }
            else
            {
                avc.nPFrames                  = 29;
                avc.nBFrames                  = 0;
                avc.eProfile                  = (OMX_VIDEO_AVCPROFILETYPE)profile;
                avc.eLevel                    = (OMX_VIDEO_AVCLEVELTYPE)level;
                avc.bUseHadamard              = OMX_FALSE;
                avc.nRefFrames                = 1;
                avc.nRefIdx10ActiveMinus1     = 1;
                avc.nRefIdx11ActiveMinus1     = 0;
                avc.bEnableUEP                = OMX_FALSE;
                avc.bEnableFMO                = OMX_FALSE;
                avc.bEnableASO                = OMX_FALSE;
                avc.bEnableRS                 = OMX_FALSE;
                avc.nAllowedPictureTypes      = 2;
                avc.bFrameMBsOnly             = OMX_FALSE;
                avc.bMBAFF                    = OMX_FALSE;
                avc.bWeightedPPrediction      = OMX_FALSE;
                avc.nWeightedBipredicitonMode = 0;
                avc.bconstIpred               = OMX_FALSE;
                avc.bDirect8x8Inference       = OMX_FALSE;
                avc.bDirectSpatialTemporal    = OMX_FALSE;
                avc.eLoopFilterMode           = OMX_VIDEO_AVCLoopFilterEnable;
                avc.bEntropyCodingCABAC       = OMX_FALSE;
                avc.nCabacInitIdc             = 0;

                OMX_RESET_STRUCT_SIZE_VERSION(&avc, OMX_VIDEO_PARAM_AVCTYPE);

                status = OMX_SetParameter(m_OMXHandle, OMX_IndexParamVideoAvc, (OMX_PTR)&avc);

                if (status != OMX_ErrorNone)
                {
                    printf("\nOMX_SetParameter of OMX_IndexParamVideoAvc failed!");
                }
            }
        }
        else if (codingType == OMX_VIDEO_CodingHEVC)
        {
            OMX_VIDEO_PARAM_HEVCTYPE hevc;

            OMX_RESET_STRUCT(&hevc, OMX_VIDEO_PARAM_HEVCTYPE);

            hevc.nPortIndex = PortIndexOut;

            status = OMX_GetParameter(m_OMXHandle, (OMX_INDEXTYPE)OMX_IndexParamVideoHevc, (OMX_PTR)&hevc);

            if (status != OMX_ErrorNone)
            {
                printf("\nOMX_GetParameter of OMX_IndexParamVideoHevc failed!");
            }
            else
            {
                hevc.eProfile = (OMX_VIDEO_HEVCPROFILETYPE)profile;
                hevc.eLevel   = (OMX_VIDEO_HEVCLEVELTYPE)level;

                OMX_RESET_STRUCT_SIZE_VERSION(&hevc, OMX_VIDEO_PARAM_HEVCTYPE);

                status = OMX_SetParameter(m_OMXHandle, (OMX_INDEXTYPE)OMX_IndexParamVideoHevc, (OMX_PTR)&hevc);

                if (status != OMX_ErrorNone)
                {
                    printf("\nOMX_SetParameter of OMX_IndexParamVideoHevc failed!");
                }
            }
        }
        else
        {
            status = OMX_ErrorBadParameter;
            printf("\nHELLOHAL3VIDEO-ERROR: Unsupported coding type!");
        }
    }

    // Set framerate
    if (status == OMX_ErrorNone)
    {
        OMX_CONFIG_FRAMERATETYPE framerate;

        OMX_RESET_STRUCT(&framerate, OMX_CONFIG_FRAMERATETYPE);

        framerate.nPortIndex = PortIndexIn;

        status = OMX_GetConfig(m_OMXHandle, OMX_IndexConfigVideoFramerate, (OMX_PTR)&framerate);
        
        if (status == OMX_ErrorNone)
        {
            FractionToQ16(framerate.xEncodeFramerate, (int)(pVideoEncoderConfig->frameRate * 2), 2);

            OMX_RESET_STRUCT_SIZE_VERSION(&framerate, OMX_CONFIG_FRAMERATETYPE);

            status = OMX_SetConfig(m_OMXHandle, OMX_IndexConfigVideoFramerate, (OMX_PTR)&framerate);

            m_frameTimestampInc = 30000; // microsecs

            if (pVideoEncoderConfig->frameRate > 60)
            {
                m_frameTimestampInc = 15000;
            }

            if (status != OMX_ErrorNone)
            {
                printf("\nHELLOHAL3VIDEO-ERROR: OMX_SetConfig of OMX_IndexConfigVideoFramerate failed!");
            }
        }
        else
        {
            printf("\nHELLOHAL3VIDEO-ERROR: OMX_GetConfig of OMX_IndexConfigVideoFramerate failed!");
        }
    }

    // Set Color aspect parameters
    if (status == OMX_ErrorNone)
    {
        android::DescribeColorAspectsParams colorParams;
        OMX_RESET_STRUCT(&colorParams, android::DescribeColorAspectsParams);
        colorParams.nPortIndex = PortIndexIn;

        status = OMX_GetConfig(m_OMXHandle, (OMX_INDEXTYPE)OMX_QTIIndexConfigDescribeColorAspects, (OMX_PTR)&colorParams);

        if (status == OMX_ErrorNone)
        {
            colorParams.sAspects.mPrimaries    = android::ColorAspects::PrimariesBT709_5;
            colorParams.sAspects.mTransfer     = android::ColorAspects::TransferSMPTE170M;
            colorParams.sAspects.mMatrixCoeffs = android::ColorAspects::MatrixBT709_5;

            OMX_RESET_STRUCT_SIZE_VERSION(&colorParams, android::DescribeColorAspectsParams);

            status = OMX_SetConfig(m_OMXHandle, (OMX_INDEXTYPE)OMX_QTIIndexConfigDescribeColorAspects, (OMX_PTR)&colorParams);

            if (status != OMX_ErrorNone)
            {
                printf("\nHELLOHAL3VIDEO-ERROR: OMX_SetConfig of OMX_QTIIndexConfigDescribeColorAspects failed!");
            }
        }
        else
        {
            printf("\nHELLOHAL3VIDEO-ERROR: OMX_GetConfig of OMX_QTIIndexConfigDescribeColorAspects failed!");
        }        
    }

    // Set bit rate
    if (status == OMX_ErrorNone)
    {
        OMX_VIDEO_PARAM_BITRATETYPE paramBitRate;
        OMX_RESET_STRUCT(&paramBitRate, OMX_VIDEO_PARAM_BITRATETYPE);

        paramBitRate.nPortIndex = PortIndexOut;

        status = OMX_GetParameter(m_OMXHandle, (OMX_INDEXTYPE)OMX_IndexParamVideoBitrate, (OMX_PTR)&paramBitRate);

        if (status == OMX_ErrorNone)
        {
            paramBitRate.eControlRate = OMX_Video_ControlRateVariable;

            if (pVideoEncoderConfig->isBitRateConstant == true)
            {
                paramBitRate.eControlRate = OMX_Video_ControlRateConstant;
            }

            paramBitRate.nTargetBitrate = TargetBitrateDefault;

            if (pVideoEncoderConfig->targetBitRate > 0)
            {
                paramBitRate.nTargetBitrate = pVideoEncoderConfig->targetBitRate;
            }

            OMX_RESET_STRUCT_SIZE_VERSION(&paramBitRate, OMX_VIDEO_PARAM_BITRATETYPE);

            status = OMX_SetParameter(m_OMXHandle, (OMX_INDEXTYPE)OMX_IndexParamVideoBitrate, (OMX_PTR)&paramBitRate);

            if (status != OMX_ErrorNone)
            {
                printf("\nHELLOHAL3VIDEO-ERROR: OMX_SetParameter of OMX_IndexParamVideoBitrate failed!");
            }
        }
        else
        {
            printf("\nHELLOHAL3VIDEO-ERROR: OMX_GetParameter of OMX_IndexParamVideoBitrate failed!");
        }
    }

    // Set/Get input port parameters
    if (status == OMX_ErrorNone)
    {
        status = SetPortParams((OMX_U32)PortIndexIn,
                               (OMX_U32)(pVideoEncoderConfig->width),
                               (OMX_U32)(pVideoEncoderConfig->height),
                               (OMX_U32)InputBufferCount,
                               (OMX_U32)(pVideoEncoderConfig->frameRate),
                               (OMX_U32*)&m_inputBufferSize,
                               (OMX_U32*)&m_inputBufferCount,
                               omxFormat);

        if (status != OMX_ErrorNone)
        {
            printf("\nHELLOHAL3VIDEO-ERROR: SetPortParams of PortIndexIn failed!");
        }
    }

    // Set/Get output port parameters
    if (status == OMX_ErrorNone)
    {
        status = SetPortParams((OMX_U32)PortIndexOut,
                               (OMX_U32)(pVideoEncoderConfig->width),
                               (OMX_U32)(pVideoEncoderConfig->height),
                               (OMX_U32)OutputBufferCount,
                               (OMX_U32)(pVideoEncoderConfig->frameRate),
                               (OMX_U32*)&m_outputBufferSize,
                               (OMX_U32*)&m_outputBufferCount,
                               omxFormat); // Not used for output port        

        if (status != OMX_ErrorNone)
        {
            printf("\nHELLOHAL3VIDEO-ERROR: SetPortParams of PortIndexOut failed!");
        }
    }

    // Allocate input / output port buffers
    if (status == OMX_ErrorNone)
    {
        m_ppInputBuffers  = (OMX_BUFFERHEADERTYPE **)malloc(sizeof(OMX_BUFFERHEADERTYPE *) * m_inputBufferCount);
        m_ppOutputBuffers = (OMX_BUFFERHEADERTYPE **)malloc(sizeof(OMX_BUFFERHEADERTYPE *) * m_outputBufferCount);

        if ((m_ppInputBuffers == NULL) || (m_ppOutputBuffers == NULL))
        {
            printf("\nHELLOHAL3VIDEO-ERROR: Allocate OMX_BUFFERHEADERTYPE ** failed");
        }

        for (uint32_t i = 0; i < m_inputBufferCount; i++)
        {
            // The OMX component i.e. the video encoder allocates the memory residing behind these buffers
            status = OMX_AllocateBuffer (m_OMXHandle, &m_ppInputBuffers[i], PortIndexIn, this, m_inputBufferSize);

            if (status != OMX_ErrorNone)
            {
                printf("\nHELLOHAL3VIDEO-ERROR: OMX_AllocateBuffer on input buffers failed");
                break;
            }
        }

        for (uint32_t i = 0; i < m_outputBufferCount; i++)
        {
            // The OMX component i.e. the video encoder allocates the memory residing behind these buffers
            status = OMX_AllocateBuffer (m_OMXHandle, &m_ppOutputBuffers[i], PortIndexOut, this, m_outputBufferSize);

            if (status != OMX_ErrorNone)
            {
                printf("\nHELLOHAL3VIDEO-ERROR: OMX_AllocateBuffer on output buffers failed");
                break;
            }
        }
    }

    if (status == OMX_ErrorNone)
    {
        status = OMX_SendCommand(m_OMXHandle, OMX_CommandStateSet, (OMX_U32)OMX_StateIdle, NULL);

        if (status != OMX_ErrorNone)
        {
            printf("\nHELLOHAL3VIDEO-ERROR: OMX_SendCommand failed");
        }
    }

    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// This function sets the input or output port parameters and gets the input or output port buffer sizes and count to allocate
// -----------------------------------------------------------------------------------------------------------------------------
OMX_ERRORTYPE VideoEncoder::SetPortParams(OMX_U32  portIndex,               ///< In or Out port
                                          OMX_U32  width,                   ///< Image width
                                          OMX_U32  height,                  ///< Image height
                                          OMX_U32  bufferCountMin,          ///< Minimum number of buffers
                                          OMX_U32  frameRate,               ///< Frame rate
                                          OMX_U32* pBufferSize,             ///< Returned buffer size
                                          OMX_U32* pBufferCount,            ///< Returned number of buffers
                                          OMX_COLOR_FORMATTYPE inputFormat) ///< Image format on the input port
{
    OMX_ERRORTYPE status = OMX_ErrorNone;
    OMX_PARAM_PORTDEFINITIONTYPE sPortDef;
    OMX_RESET_STRUCT(&sPortDef, OMX_PARAM_PORTDEFINITIONTYPE);    

    if ((pBufferSize != NULL) && (pBufferCount != NULL))
    {
        sPortDef.nPortIndex = portIndex;

        status = OMX_GetParameter(m_OMXHandle, OMX_IndexParamPortDefinition, (OMX_PTR)&sPortDef);

        if (status != OMX_ErrorNone)
        {
            printf("\nHELLOHAL3VIDEO-ERROR: OMX_GetParameter OMX_IndexParamPortDefinition failed!");
        }
    }
    else
    {
        printf("\nHELLOHAL3VIDEO-ERROR: Buffer error : NULL pointer");
        status = OMX_ErrorBadParameter;
    }
    
    // Get the port buffer count and size
    if (status == OMX_ErrorNone)
    {
        FractionToQ16(sPortDef.format.video.xFramerate,(int)(frameRate * 2), 2);

        sPortDef.format.video.nFrameWidth  = width;
        sPortDef.format.video.nFrameHeight = height;
        sPortDef.format.video.nBitrate     = BitrateDefault;

        if (portIndex == PortIndexIn)
        {
            sPortDef.format.video.eColorFormat = inputFormat;
        }

        OMX_RESET_STRUCT_SIZE_VERSION(&sPortDef, OMX_PARAM_PORTDEFINITIONTYPE);

        status = OMX_SetParameter(m_OMXHandle, OMX_IndexParamPortDefinition, (OMX_PTR)&sPortDef);

        if (status != OMX_ErrorNone)
        {
            printf("\nHELLOHAL3VIDEO-ERROR: OMX_SetParameter OMX_IndexParamPortDefinition failed!");
        }
    }

    // Set the port parameters
    if (status == OMX_ErrorNone)
    {
        if (bufferCountMin < sPortDef.nBufferCountMin)
        {
            bufferCountMin = sPortDef.nBufferCountMin;
        }

        sPortDef.nBufferCountActual = bufferCountMin;
        sPortDef.nBufferCountMin    = bufferCountMin;

        OMX_RESET_STRUCT_SIZE_VERSION(&sPortDef, OMX_PARAM_PORTDEFINITIONTYPE);

        status = OMX_SetParameter(m_OMXHandle, OMX_IndexParamPortDefinition, (OMX_PTR)&sPortDef);

        if (status != OMX_ErrorNone)
        {
            printf("\nHELLOHAL3VIDEO-ERROR: OMX_SetParameter OMX_IndexParamPortDefinition failed!");
        }
    }
    
    if (status == OMX_ErrorNone)
    {
        status = OMX_GetParameter(m_OMXHandle, OMX_IndexParamPortDefinition, (OMX_PTR)&sPortDef);

        if (status != OMX_ErrorNone)
        {
            printf("\nHELLOHAL3VIDEO-ERROR: OMX_GetParameter OMX_IndexParamPortDefinition failed!");
        }
    }
    
    if (status == OMX_ErrorNone)
    {
        *pBufferCount = sPortDef.nBufferCountActual;
        *pBufferSize  = sPortDef.nBufferSize;
    }
    else
    {
        *pBufferCount = 0;
        *pBufferSize  = 0;
    }    

    return status;
}
// -----------------------------------------------------------------------------------------------------------------------------
// The client calls this interface function to pass in a YUV image frame to be encoded
// -----------------------------------------------------------------------------------------------------------------------------
void VideoEncoder::ProcessFrameToEncode(int frameNumber, BufferInfo* pBufferToEncode)
{
    OMXInPortThreadMessageData* pOMXThreadMessageData = new OMXInPortThreadMessageData;

    memset(pOMXThreadMessageData, 0, sizeof(OMXInPortThreadMessageData));

    pOMXThreadMessageData->frameNumber = frameNumber;
    pOMXThreadMessageData->pInputFrame = pBufferToEncode;
    pOMXThreadMessageData->pOMXBuffer  = m_ppInputBuffers[m_nextInputBufferIndex++];

    m_nextInputBufferIndex = (m_nextInputBufferIndex % m_inputBufferCount);

    // Mutex is required for msgQueue access from here and from within the thread wherein it will be de-queued
    pthread_mutex_lock(&m_OMXInPortThread.mutex);
    // Queue up work for thread "ThreadProcessOMXInputPort"
    m_OMXInPortThread.msgQueue.push_back((void*)pOMXThreadMessageData);
    pthread_cond_signal(&m_OMXInPortThread.cond);
    pthread_mutex_unlock(&m_OMXInPortThread.mutex);
}

// -----------------------------------------------------------------------------------------------------------------------------
// OMX output thread calls this function to process the OMX component's output encoded buffer
// -----------------------------------------------------------------------------------------------------------------------------
void VideoEncoder::ProcessEncodedFrame(OMX_BUFFERHEADERTYPE* pEncodedFrame)
{
    fwrite(pEncodedFrame->pBuffer, 1, pEncodedFrame->nFilledLen, m_pVideoFilehandle);
}

// -----------------------------------------------------------------------------------------------------------------------------
// This function performs any work necessary to start receiving encoding frames from the client
// -----------------------------------------------------------------------------------------------------------------------------
void VideoEncoder::Start()
{
    pthread_condattr_t attr;
    pthread_condattr_init(&attr);
    pthread_condattr_setclock(&attr, CLOCK_MONOTONIC);
    pthread_mutex_init(&m_OMXInPortThread.mutex, NULL);
    pthread_mutex_init(&m_OMXOutPortThread.mutex, NULL);
    pthread_cond_init(&m_OMXInPortThread.cond, &attr);
    pthread_cond_init(&m_OMXOutPortThread.cond, &attr);
    pthread_condattr_destroy(&attr);

    // This thread will process the outputs i.e. the encoded frames from the OMX component
    pthread_attr_t resultAttr;
    pthread_attr_init(&resultAttr);
    pthread_attr_setdetachstate(&resultAttr, PTHREAD_CREATE_JOINABLE);
    pthread_create(&(m_OMXOutPortThread.thread), &resultAttr, ThreadProcessOMXOutputPort, &m_OMXOutPortThread);
    pthread_attr_destroy(&resultAttr);

    // This thread will provide encoding frames as input to the OMX component
    pthread_attr_t requestAttr;
    pthread_attr_init(&requestAttr);
    pthread_attr_setdetachstate(&requestAttr, PTHREAD_CREATE_JOINABLE);
    pthread_create(&(m_OMXInPortThread.thread), &requestAttr, ThreadProcessOMXInputPort, &m_OMXInPortThread);
    pthread_attr_destroy(&resultAttr);
}

// -----------------------------------------------------------------------------------------------------------------------------
// This function is called by the client to indicate that no more frames will be sent for encoding
// -----------------------------------------------------------------------------------------------------------------------------
void VideoEncoder::Stop()
{
    if (m_OMXInPortThread.pVideoEncoder != NULL)
    {
        m_OMXInPortThread.stop  = true;
        pthread_join(m_OMXInPortThread.thread, NULL);
        pthread_cond_signal(&m_OMXInPortThread.cond);
        pthread_mutex_unlock(&m_OMXInPortThread.mutex);
        pthread_mutex_destroy(&m_OMXInPortThread.mutex);
        pthread_cond_destroy(&m_OMXInPortThread.cond);

        // The thread wont finish and the "join" call will not return till the last expected encoded frame is received from
        // the encoder OMX component
        m_OMXOutPortThread.stop = true;
        pthread_join(m_OMXOutPortThread.thread, NULL);
        pthread_cond_signal(&m_OMXOutPortThread.cond);
        pthread_mutex_unlock(&m_OMXOutPortThread.mutex);
        pthread_mutex_destroy(&m_OMXOutPortThread.mutex);
        pthread_cond_destroy(&m_OMXOutPortThread.cond);
    }
}

// -----------------------------------------------------------------------------------------------------------------------------
// Function called by the OMX component for event handling
// -----------------------------------------------------------------------------------------------------------------------------
OMX_ERRORTYPE OMXEventHandler(OMX_IN OMX_HANDLETYPE hComponent,     ///< OMX component handle
                              OMX_IN OMX_PTR        pAppData,       ///< Any private app data
                              OMX_IN OMX_EVENTTYPE  eEvent,         ///< Event identifier
                              OMX_IN OMX_U32        nData1,         ///< Data 1
                              OMX_IN OMX_U32        nData2,         ///< Data 2
                              OMX_IN OMX_PTR        pEventData)     ///< Event data
{
    OMX_ERRORTYPE status = OMX_ErrorNone;

    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Function called by the OMX component indicating it has completed consuming our YUV frame for encoding
// -----------------------------------------------------------------------------------------------------------------------------
OMX_ERRORTYPE OMXEmptyBufferHandler(OMX_IN OMX_HANDLETYPE        hComponent,    ///< OMX component handle
                                    OMX_IN OMX_PTR               pAppData,      ///< Any private app data
                                    OMX_IN OMX_BUFFERHEADERTYPE* pBuffer)       ///< Buffer that has been emptied
{
    OMX_ERRORTYPE status = OMX_ErrorNone;

    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Function called by the OMX component to give us the encoded frame. Since this is a callback we dont do much work but simply
// prepare work that will be done by the worker threads
// -----------------------------------------------------------------------------------------------------------------------------
OMX_ERRORTYPE OMXFillHandler(OMX_OUT OMX_HANDLETYPE        hComponent,  ///< OMX component handle
                             OMX_OUT OMX_PTR               pAppData,    ///< Any private app data
                             OMX_OUT OMX_BUFFERHEADERTYPE* pBuffer)     ///< Buffer that has been filled by OMX component
{
    static int64_t lastFrameNsecs = 0;
    static struct timespec temp;
    clock_gettime(CLOCK_REALTIME, &temp);
    int64_t currentNsecs = ((temp.tv_sec*1e9) + temp.tv_nsec);

    if (lastFrameNsecs != 0)
    {
        g_totalNsecsEncodedFrames += (currentNsecs - lastFrameNsecs);
    }

    lastFrameNsecs = currentNsecs;

    OMX_ERRORTYPE  status = OMX_ErrorNone;
    VideoEncoder*  pVideoEncoder = (VideoEncoder*)pAppData;
    OMXThreadData* pOMXOutPortThread = pVideoEncoder->GetOMXOutputThreadData();
    OMXOutPortThreadMessageData* pMessageData = new OMXOutPortThreadMessageData;

    memset(pMessageData, 0, sizeof(OMXOutPortThreadMessageData));
    pMessageData->pOMXBuffer = pBuffer;

    pthread_mutex_lock(&pOMXOutPortThread->mutex);
    // Queue up work for thread "ThreadProcessOMXOutputPort"
    pOMXOutPortThread->msgQueue.push_back((void*)pMessageData);
    pthread_cond_signal(&pOMXOutPortThread->cond);
    pthread_mutex_unlock(&pOMXOutPortThread->mutex);

    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// This thread queues up work on the input port of the OMX component i.e. the video encoder
// -----------------------------------------------------------------------------------------------------------------------------
void* ThreadProcessOMXInputPort(void* pData)
{
    // Set thread priority
    pid_t tid = syscall(SYS_gettid);
    int which = PRIO_PROCESS;
    int nice  = -10;
    
    setpriority(which, tid, nice);

    OMXThreadData* pOMXThreadData      = (OMXThreadData*)pData;
    VideoEncoder*  pVideoEncoder       = pOMXThreadData->pVideoEncoder;
    OMX_ERRORTYPE  status              = OMX_ErrorNone;
    OMX_HANDLETYPE omxHandle           = pVideoEncoder->GetOMXHandle();
    int            frameNumber         = 0;
    int64_t        timestamp           = 0;
    int64_t        timestampIncrements = pOMXThreadData->pVideoEncoder->GetTimestampIncrements();

    // The condition of the while loop is such that this thread will not terminate till it consumes the last image frame
    // given by the camera module
    while ((pOMXThreadData->stop == false) || !pOMXThreadData->msgQueue.empty())
    {
        pthread_mutex_lock(&pOMXThreadData->mutex);

        if (pOMXThreadData->msgQueue.empty()) 
        {
            struct timespec tv;
            clock_gettime(CLOCK_MONOTONIC, &tv);
            tv.tv_sec += 1;

            // Go to a temporary small sleep waiting for the camera module frame to arrive
            if(pthread_cond_timedwait(&pOMXThreadData->cond, &pOMXThreadData->mutex, &tv) != 0) 
            {
                pthread_mutex_unlock(&pOMXThreadData->mutex);
                continue;
            }
        }

        // Coming here means we have a YUV frame to process

        OMXInPortThreadMessageData* pOMXThreadMessageData = (OMXInPortThreadMessageData*)pOMXThreadData->msgQueue.front();
        pOMXThreadData->msgQueue.pop_front();
        pthread_mutex_unlock(&pOMXThreadData->mutex);

        frameNumber = pOMXThreadMessageData->frameNumber;

        BufferInfo* pBufferInfo  = pOMXThreadMessageData->pInputFrame;
        uint8_t*    pDestAddress = (uint8_t*)pOMXThreadMessageData->pOMXBuffer->pBuffer;
        uint8_t*    pSrcAddress  = (uint8_t*)pBufferInfo->vaddr;

        // Copy the YUV frame data into the OMX component input port OMX buffer. The data needs to be provided to the encoder
        // in a compact manner i.e. Y data immediately followed by UV data and (stride == width)

        memcpy(pDestAddress, pSrcAddress, pBufferInfo->size);

        pOMXThreadMessageData->pOMXBuffer->nFilledLen = pBufferInfo->size;
        pOMXThreadMessageData->pOMXBuffer->nTimeStamp = timestamp;
        timestamp += timestampIncrements;

        status = OMX_EmptyThisBuffer(omxHandle, pOMXThreadMessageData->pOMXBuffer);

        if (status != OMX_ErrorNone)
        {
            printf("\nHELLOHAL3VIDEO-ERROR: OMX_EmptyThisBuffer failed for framebuffer: %d", frameNumber);
        }

        delete pOMXThreadMessageData;
    }

    pVideoEncoder->GetOMXOutputThreadData()->lastFrameNumber = frameNumber;
    printf("\n========= Last frame sent for encoding: %d", frameNumber);
    fflush(stdout);

    return NULL;
}

// -----------------------------------------------------------------------------------------------------------------------------
// This thread function processes the encoded buffers available on the OMX component's output port
// -----------------------------------------------------------------------------------------------------------------------------
void* ThreadProcessOMXOutputPort(void* pData)
{
    // Set thread priority
    pid_t tid   = syscall(SYS_gettid);
    int   which = PRIO_PROCESS;
    int   nice  = -10;
    
    setpriority(which, tid, nice);

    OMXThreadData* pOMXThreadData = (OMXThreadData*)pData;
    OMX_ERRORTYPE  status         = OMX_ErrorNone;
    VideoEncoder*  pVideoEncoder  = pOMXThreadData->pVideoEncoder;
    int frameNumber = 0;

    // The condition of the while loop is such that this thread will not terminate till it receives the last expected encoded
    // frame from the OMX component
    while ((pOMXThreadData->stop == false)         ||
           (pOMXThreadData->lastFrameNumber == -1) ||
           (pOMXThreadData->lastFrameNumber > frameNumber))
    {
        pthread_mutex_lock(&pOMXThreadData->mutex);

        if (pOMXThreadData->msgQueue.empty()) 
        {
            struct timespec tv;
            clock_gettime(CLOCK_MONOTONIC, &tv);
            tv.tv_sec += 1;

            // Go to a temporary small sleep waiting for the encoded frame to arrive
            if(pthread_cond_timedwait(&pOMXThreadData->cond, &pOMXThreadData->mutex, &tv) != 0) 
            {
                pthread_mutex_unlock(&pOMXThreadData->mutex);
                continue;
            }
        }

        // Coming here means we have a encoded frame to process

        OMXOutPortThreadMessageData* pOMXThreadMessageData = (OMXOutPortThreadMessageData*)pOMXThreadData->msgQueue.front();

        pOMXThreadData->msgQueue.pop_front();
        pthread_mutex_unlock(&pOMXThreadData->mutex);

        // pOMXThreadMessageData->pOMXBuffer contains the encoded frame data
        pVideoEncoder->ProcessEncodedFrame(pOMXThreadMessageData->pOMXBuffer);

        // Since we processed the OMX buffer we can immediately recycle it by sending it to the output port of the OMX
        // component
        status = OMX_FillThisBuffer(pVideoEncoder->GetOMXHandle(), pOMXThreadMessageData->pOMXBuffer);

        delete pOMXThreadMessageData;

        if (status != OMX_ErrorNone)
        {
            printf("\nHELLOHAL3VIDEO-ERROR: OMX_FillThisBuffer resulted in error for frame %d", frameNumber);
        }

        frameNumber++;
    }

    printf("\n========= Last frame encoded: %d", frameNumber - 1);
    printf("\n========= Total encoding time (msecs): %lld", g_totalNsecsEncodedFrames/1000000);
    printf("\n========= Average encoding time per frame (msecs): %lld", ((g_totalNsecsEncodedFrames/1000000)/frameNumber));
    fflush(stdout);

    return NULL;
}
