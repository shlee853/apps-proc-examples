/*******************************************************************************************************************************
 *
 * Copyright (c) 2019 ModalAI, Inc.
 *
 ******************************************************************************************************************************/
#include <stdio.h>
#include "hello_hal3_camera.h"

// -----------------------------------------------------------------------------------------------------------------------------
// Constructor
// -----------------------------------------------------------------------------------------------------------------------------
CameraHAL3::CameraHAL3()
{
    m_moduleCallbacks = {CameraDeviceStatusChange, TorchModeStatusChange};

    for (uint32_t i = 0; i < MaxCameras; i++)
    {
        m_pPerCameraMgr[i] = NULL;
    }
}

// -----------------------------------------------------------------------------------------------------------------------------
// Performs any one time initialization. This function should only be called once.
// -----------------------------------------------------------------------------------------------------------------------------
int CameraHAL3::Initialize()
{
    int status = hw_get_module(CAMERA_HARDWARE_MODULE_ID, (const hw_module_t**)&m_pCameraModule);

    if (status == 0)
    {
        printf("\nSUCCESS: Camera module opened");
    }
    else
    {
        printf("\nERROR: Cannot open camera module!");
    }

    if (status == 0)
    {
        if (m_pCameraModule->init != NULL)
        {
            status = m_pCameraModule->init();
        }
    }

    if (status == 0)
    {
        m_numCameras = m_pCameraModule->get_number_of_cameras();

        for (int i = 0 ;i < m_numCameras;i++)
        {
            // This gives the camera's fixed characteristics that can be extracted from the camera_metadata
            // "info.static_camera_characteristics"
            status = m_pCameraModule->get_camera_info(i, &m_cameraInfo[i]);

            if (status == 0)
            {
                printf("\nCamera Id: %d Facing: %d", i, m_cameraInfo[i].facing);
            }
            else
            {
                fprintf(stderr, "\nError getting info for camera: %d", i);
                break;;
            }
        }
    }

    if (status == 0)
    {
        status = m_pCameraModule->set_callbacks(&m_moduleCallbacks);
    }

    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// This function opens the camera and starts sending the capture requests
// -----------------------------------------------------------------------------------------------------------------------------
int CameraHAL3::Start(int           cameraid,           ///< Camera id to open
                      int           width,              ///< Image buffer width
                      int           height,             ///< Image buffer height
                      PreviewFormat format,             ///< Image buffer format
                      CameraMode    mode,               ///< Preview / Video
                      const char*   pVideoFilename,     ///< Video filename for Video mode
                      int           dumpPreviewFrames,  ///< Number of preview frames to dump
                      string*       frameFifo)          ///< Named FIFO to place frames into
{
    int status = -EINVAL;

    if (cameraid < m_numCameras)
    {
        m_pPerCameraMgr[cameraid] = new PerCameraMgr;

        // Initialize the per camera manager
        status = m_pPerCameraMgr[cameraid]->Initialize(m_pCameraModule,
                                                       &m_cameraInfo[cameraid],
                                                       cameraid,
                                                       width,
                                                       height,
                                                       format,
                                                       mode,
                                                       pVideoFilename,
                                                       dumpPreviewFrames,
                                                       frameFifo);

        if (status == 0)
        {
            // Start the camera which will start sending requests and processing results from the camera module
            status = m_pPerCameraMgr[cameraid]->Start();
        }
    }
    else
    {
        printf("\nInvalid camera id %d. There are only %d cameras", cameraid, m_numCameras);
    }

    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// This function informs the per camera manager to stop the camera and stop sending any more requests
// -----------------------------------------------------------------------------------------------------------------------------
void CameraHAL3::Stop()
{
    for (uint32_t i = 0; i < MaxCameras; i++)
    {
        if (m_pPerCameraMgr[i] != NULL)
        {
            m_pPerCameraMgr[i]->Stop();
            m_pPerCameraMgr[i] = NULL;
        }
    }
}
