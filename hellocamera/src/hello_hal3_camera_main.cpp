/*******************************************************************************************************************************
 *
 * Copyright (c) 2019 ModalAI, Inc.
 *
 ******************************************************************************************************************************/
#include <errno.h>
#include <getopt.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hello_hal3_camera.h"

// Function prototypes
void PrintHelpMessage();
int  ErrorCheck(int numInputsScanned, const char* pOptionName);
int  ParseArgs(int         argc,
               char* const pArgv[],
               int*        pCameraid,
               int*        pWidth,
               int*        pHeight,
               int*        pFormat,
               int*        pDumpPreviewFrames,
               string**    pFrameFifo);

void CtrlCHandler(int dummy);

volatile int g_keepRunning = 1;

// -----------------------------------------------------------------------------------------------------------------------------
// Main
// -----------------------------------------------------------------------------------------------------------------------------
int main(int argc, char* const argv[])
{
    CameraHAL3* pCameraHAL3 = new CameraHAL3;
    int         status;
    // All the default values can be overwritten with the command line args. If the command line args is not specified, the
    // defaults are used
    int         cameraid = 0;
    int         width    = 640;
    int         height   = 480;
    int         format   = PreviewFormatNV21;
    int         mode     = CameraModePreview;
    int         dumpPreviewFrames = 0;
    string      *frameFifo = NULL;

    signal(SIGINT, CtrlCHandler);

    status = ParseArgs(argc, argv, &cameraid, &width, &height, &format, &dumpPreviewFrames, &frameFifo);

    if (status == 0)
    {
        printf("\nCamera id: %d", cameraid);
        printf("\nImage width: %d", width);
        printf("\nImage height: %d", height);
        printf("\nNumber of frames to dump: %d", dumpPreviewFrames);
        if (frameFifo) printf("\nFrame FIFO: %s", frameFifo->c_str());
        printf("\nCamera mode: preview");

        status = pCameraHAL3->Initialize();
    }
    else
    {
        PrintHelpMessage();
    }

    if (status == 0)
    {
        // Making this function call will start the camera (cameraid) and it will start streaming frames
        status = pCameraHAL3->Start(cameraid,
                                    width,
                                    height,
                                    (PreviewFormat)format,
                                    (CameraMode)mode,
                                    NULL,
                                    dumpPreviewFrames,
                                    frameFifo);
    }

    if (status == 0)
    {
        printf("\n\nCamera HAL3 test is now running");
        fflush(stdout);

        // The camera keeps producing frames till the user presses Ctrl+C to terminate the program
        while (g_keepRunning)
        {
            usleep(1e5);
        }

        printf("\nCamera HAL3 test is now stopping");
        pCameraHAL3->Stop();
        printf("\nCamera HAL3 test is done");
    }

    if (frameFifo) delete frameFifo;
    delete pCameraHAL3;

    printf("\n\n");
    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Parses the command line arguments to the main function
// -----------------------------------------------------------------------------------------------------------------------------
int ParseArgs(int         argc,
              char* const pArgv[],
              int*        pCameraid,
              int*        pWidth,
              int*        pHeight,
              int*        pFormat,
              int*        pDumpPreviewFrames,
              string**    pFrameFifo)
{
    static struct option LongOptions[] =
    {
        {"cameraid",     optional_argument, 0, 'c'},
        {"width",        optional_argument, 0, 'W'},
        {"height",       optional_argument, 0, 'H'},
        {"format",       optional_argument, 0, 'f'},
        {"dumppreview",  optional_argument, 0, 'd'},
        {"framefifo",    optional_argument, 0, 'l'},
        {"help",         no_argument,       0, 'h'},
        {0, 0, 0, 0                               }
    };

    int numInputsScanned = 0;
    int optionIndex      = 0;
    int status           = 0;
    int option;

    while ((status == 0) && (option = getopt_long_only (argc, pArgv, ":c:W:H:f:m:v:d:l:h", &LongOptions[0], &optionIndex)) != -1)
    {
        switch(option)
        {
            case 'c':
                numInputsScanned = sscanf(optarg, "%d", pCameraid);

                if (ErrorCheck(numInputsScanned, LongOptions[optionIndex].name) != 0)
                {
                    printf("\nNo cameraId specified!");
                    status = -EINVAL;
                }

                break;

            case 'W':
                numInputsScanned = sscanf(optarg, "%d", pWidth);

                if (ErrorCheck(numInputsScanned, LongOptions[optionIndex].name) != 0)
                {
                    printf("\nNo width specified");
                    status = -EINVAL;
                }

                break;

            case 'H':
                numInputsScanned = sscanf(optarg, "%d", pHeight);

                if (ErrorCheck(numInputsScanned, LongOptions[optionIndex].name) != 0)
                {
                    printf("\nNo height specified");
                    status = -EINVAL;
                }

                break;

            case 'd':
                numInputsScanned = sscanf(optarg, "%d", pDumpPreviewFrames);

                if (ErrorCheck(numInputsScanned, LongOptions[optionIndex].name) != 0)
                {
                    printf("\nNo preview dump frames specified");
                    status = -EINVAL;
                }

                break;

            case 'l':
                // The frame FIFO is useful to pass frames into a Docker container
                // since the container may not be able to access the cameras directly.
                if (strlen(optarg) != 0) {
                    numInputsScanned = 1;
                    *pFrameFifo = new string(optarg);
                } else {
                    *pFrameFifo = NULL;
                }

                if (ErrorCheck(numInputsScanned, LongOptions[optionIndex].name) != 0)
                {
                    printf("\nNo frame fifo specified");
                    status = -EINVAL;
                }

                break;

            case 'f':
                numInputsScanned = sscanf(optarg, "%d", pFormat);

                if (ErrorCheck(numInputsScanned, LongOptions[optionIndex].name) == 0)
                {
                    if ((*pFormat != PreviewFormatNV21) && (*pFormat != PreviewFormatRAW8))
                    {
                        printf("\nUnsupported format specified!");
                        status = -EINVAL;
                    }
                }
                else
                {
                    printf("\nNo format specified");
                    status = -EINVAL;
                }

                break;

            case 'h':
                status = -EINVAL; // This will have the effect of printing the help message and exiting the program
                break;

            // Unknown argument
            case '?':
            default:
                printf("\nInvalid argument passed!");
                status = -EINVAL;
                break;
        }
    }

    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Print the help message
// -----------------------------------------------------------------------------------------------------------------------------
void PrintHelpMessage()
{
    printf("\n\nCommand line arguments are as follows:\n");
    printf("\n-c : camera id (Default 0)");
    printf("\n-W : frame width (Default 640)");
    printf("\n-H : frame height (Default 480)");
    printf("\n-f : frame format (Default preview format nv21)");
    printf("\n\t0: NV21");
    printf("\n\t1: Raw8");
    printf("\n-d : Dump 'n' preview frames (Default 0)");
    printf("\n-l : Create a named FIFO. Frames will be put into the FIFO");
    printf("\n\t<Name of the FIFO> e.g. \"/dev/frame0\"");
    printf("\n-h : Print this help message");
    printf("\n\nFor example: hello_hal3_camera -c 0 -W 1920 -H 1080 -f 0 -d 10");
}

// -----------------------------------------------------------------------------------------------------------------------------
// Check for error in parsing the arguments
// -----------------------------------------------------------------------------------------------------------------------------
int ErrorCheck(int numInputsScanned, const char* pOptionName)
{
    int error = 0;

    if (numInputsScanned != 1)
    {
        fprintf(stderr, "ERROR: Invalid argument for %s option\n", pOptionName);
        error = -1;
    }

    return error;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Ctrl+C handler that will stop the camera and exit the program gracefully
// -----------------------------------------------------------------------------------------------------------------------------
void CtrlCHandler(int dummy)
{
    g_keepRunning = 0;
    signal(SIGINT, NULL);
}
