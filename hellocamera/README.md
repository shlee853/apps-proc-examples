# HelloCamera sample for preview using the HAL3 interface

## Dependencies:
----------------
1. [voxl-armcl](https://gitlab.com/voxl-public/voxl-armcl)
2. [voxl-opencv-3-4-6](https://gitlab.com/voxl-public/voxl-opencv-3-4-6)

## Build sample project:
-----------------------
1. Requires the voxl-emulator (found [here](https://gitlab.com/voxl-public/voxl-docker)) to run docker ARM image
    * cd [Path To]/voxl-docker
    * voxl-docker -d [Path To]/apps-proc-examples/hellocamera
2. Build project binary:
    * make
3. Push binary to target:
    * adb push hello_hal3_camera /bin
4. Run on target:
    * adb shell
    * hello_hal3_camera -h
    * hello_hal3_camera -c 0 -W 1920 -H 1080 -f 0 -d 5
    * Press Ctrl+C to stop the program anytime 
6. Preview files will be dumped in the current folder on the target (Filenames: image_preview_XXX)
7. Images can be viewed on "http://rawpixels.net/"

