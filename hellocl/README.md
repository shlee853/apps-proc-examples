# HelloCL Sample for simple OpenCL use case

## Build sample project:
---------------------
1. Requires the voxl-emulator (found [here](https://gitlab.com/voxl-public/voxl-docker)) to run docker ARM image
    * cd [Path To]/voxl-docker
    * voxl-docker -d [Path To]/apps-proc-examples/hellocl
2. Create build directory:
    * mkdir build
    * cd build
3. Build project binary:
    * cmake ../
    * make
4. Push binary to target:
    * adb push hellocl /home/root
4. Run on target:
    * adb shell /home/root/hellocl
